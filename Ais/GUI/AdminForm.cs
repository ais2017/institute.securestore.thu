﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AisSimpleProcess;
using Ais;


namespace GUI
{
    public partial class AdminForm : Form
    {
        private ActualInterface Something;

        public AdminForm()
        {
            Something = new WorkClass(new HistoryDatabaseClass(), new DocumentDatabaseClass(),
                new RequestDatabaseClass(), new KeysWorkClass());
            InitializeComponent();

        }

        private void downloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HistoryTable.Rows.Clear();
            List< HistorySimpleStruct > IWTD = Something.ReturnHistoryAll();
            if (IWTD != null)
            {
                foreach (HistorySimpleStruct Desc in IWTD)
                {
                    HistoryTable.Rows.Add(Desc.type, Desc.DocumentNumber, Desc.DocumentName, Desc.Date);
                }
            }
        }

        private void getRequestsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RequestTable.Rows.Clear();
            List<RequestSimpleStruct> NEWIWTD = Something.ReturnRequests();
            if (NEWIWTD != null)
            {
                foreach (RequestSimpleStruct NEWDesc in NEWIWTD)
                {
                    if (NEWDesc.Status == "Considered")
                        RequestTable.Rows.Add(NEWDesc.ReqNumber, NEWDesc.EmployeeNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "Approve", "Decline", "CheckDS");
                    else
                        RequestTable.Rows.Add(NEWDesc.ReqNumber, NEWDesc.EmployeeNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "PLS NO", "PLS NO", "CheckDS");
                }
            }
        }

        private void RequestTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                Something.ApproveRequest((long)RequestTable.CurrentRow.Cells[0].Value);
                //tbh this is stupid AF
                //RequestTable[RequestTable.CurrentRow.Index, 5].Value = "Accepted";
                //RequestTable[RequestTable.CurrentRow.Index, 6].Value = "PLS NO";
                //RequestTable[RequestTable.CurrentRow.Index, 7].Value = "PLS NO";

                RequestTable.Rows.Clear();
                List<RequestSimpleStruct> NEWIWTD = Something.ReturnRequests();
                foreach (RequestSimpleStruct NEWDesc in NEWIWTD)
                {
                    if (NEWDesc.Status == "Considered")
                        RequestTable.Rows.Add(NEWDesc.ReqNumber, NEWDesc.EmployeeNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "Approve", "Decline", "CheckDS");
                    else
                        RequestTable.Rows.Add(NEWDesc.ReqNumber, NEWDesc.EmployeeNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "PLS NO", "PLS NO", "CheckDS");
                }

            }

            if (e.ColumnIndex == 7)
            {
                Something.DeclineRequest((long)RequestTable.CurrentRow.Cells[0].Value);
                //tbh this is stupid AF
                //RequestTable[RequestTable.CurrentRow.Index, 5].Value = "Declined";
                //RequestTable[RequestTable.CurrentRow.Index, 6].Value = "PLS NO";
                //RequestTable[RequestTable.CurrentRow.Index, 7].Value = "PLS NO";

                RequestTable.Rows.Clear();
                List<RequestSimpleStruct> NEWIWTD = Something.ReturnRequests();
                foreach (RequestSimpleStruct NEWDesc in NEWIWTD)
                {
                    if (NEWDesc.Status == "Considered")
                        RequestTable.Rows.Add(NEWDesc.ReqNumber, NEWDesc.EmployeeNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "Approve", "Decline", "CheckDS");
                    else
                        RequestTable.Rows.Add(NEWDesc.ReqNumber, NEWDesc.EmployeeNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "PLS NO", "PLS NO", "CheckDS");
                }
            }

            if (e.ColumnIndex == 8)
            {
                if(Something.CheckRequestSign((long)RequestTable.CurrentRow.Cells[0].Value)) 
                {
                    MessageBox.Show(
                        "Digital signature is ok",
                        "Check Ressult",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information,
                        MessageBoxDefaultButton.Button1,
                        MessageBoxOptions.DefaultDesktopOnly);
                }
                else
                {
                    MessageBox.Show(
                        "Digital signature is NOT ok",
                        "Check Ressult",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information,
                        MessageBoxDefaultButton.Button1,
                        MessageBoxOptions.DefaultDesktopOnly);
                }
            }
        }

        private void DocumentTableAdmin_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                if (Something.CheckLocalSign((long)DocumentTableAdmin.CurrentRow.Cells[0].Value))
                {
                    MessageBox.Show(
                        "Digital signature is ok",
                        "Check Ressult",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information,
                        MessageBoxDefaultButton.Button1,
                        MessageBoxOptions.DefaultDesktopOnly);
                }
                else
                {
                    MessageBox.Show(
                        "Digital signature is NOT ok",
                        "Check Ressult",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information,
                        MessageBoxDefaultButton.Button1,
                        MessageBoxOptions.DefaultDesktopOnly);
                }
            }
        }

        private void getDocumentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DocumentTableAdmin.Rows.Clear();
            List<DocumentSimpleStruct> Hello = Something.ReturnSimpleDocumentsAll();
            if (Hello != null)
            {
                foreach (DocumentSimpleStruct NEWDesc in Hello)
                {
                    DocumentTableAdmin.Rows.Add(NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.AddTime, NEWDesc.Type, NEWDesc.AuthorName, NEWDesc.AuthorSecond, "⊂(▀¯▀⊂)");
                }
            }
        }
    }
}
