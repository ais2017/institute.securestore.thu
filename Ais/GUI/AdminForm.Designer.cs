﻿namespace GUI
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Tabs = new System.Windows.Forms.TabControl();
            this.HistoryTab = new System.Windows.Forms.TabPage();
            this.HistoryTable = new System.Windows.Forms.DataGridView();
            this.TypeColumnHistory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentNumberColumnHistory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentNameColumnHistory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateTimeColumnHistory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RequestsTab = new System.Windows.Forms.TabPage();
            this.RequestTable = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.downloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getRequestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ApproveButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DeclineButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.CheckDSButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DocumentsAdminTab = new System.Windows.Forms.TabPage();
            this.DocumentTableAdmin = new System.Windows.Forms.DataGridView();
            this.DocumentTableAdminDocNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentTableAdminDocName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentTableAdminDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentTableAdminTypeDoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentTableAdminAuthorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentTableAdminAuthorNameSecond = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentTableAdminCheck = new System.Windows.Forms.DataGridViewButtonColumn();
            this.getDocumentsButtonAdmin = new System.Windows.Forms.ToolStripMenuItem();
            this.Tabs.SuspendLayout();
            this.HistoryTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryTable)).BeginInit();
            this.RequestsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RequestTable)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.DocumentsAdminTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentTableAdmin)).BeginInit();
            this.SuspendLayout();
            // 
            // Tabs
            // 
            this.Tabs.Controls.Add(this.HistoryTab);
            this.Tabs.Controls.Add(this.RequestsTab);
            this.Tabs.Controls.Add(this.DocumentsAdminTab);
            this.Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabs.Location = new System.Drawing.Point(0, 24);
            this.Tabs.Name = "Tabs";
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(1108, 555);
            this.Tabs.TabIndex = 0;
            // 
            // HistoryTab
            // 
            this.HistoryTab.Controls.Add(this.HistoryTable);
            this.HistoryTab.Location = new System.Drawing.Point(4, 22);
            this.HistoryTab.Name = "HistoryTab";
            this.HistoryTab.Padding = new System.Windows.Forms.Padding(3);
            this.HistoryTab.Size = new System.Drawing.Size(1100, 529);
            this.HistoryTab.TabIndex = 0;
            this.HistoryTab.Text = "HistoryTab";
            this.HistoryTab.UseVisualStyleBackColor = true;
            // 
            // HistoryTable
            // 
            this.HistoryTable.AllowUserToAddRows = false;
            this.HistoryTable.AllowUserToDeleteRows = false;
            this.HistoryTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HistoryTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TypeColumnHistory,
            this.DocumentNumberColumnHistory,
            this.DocumentNameColumnHistory,
            this.DateTimeColumnHistory});
            this.HistoryTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HistoryTable.Location = new System.Drawing.Point(3, 3);
            this.HistoryTable.Name = "HistoryTable";
            this.HistoryTable.ReadOnly = true;
            this.HistoryTable.Size = new System.Drawing.Size(1094, 523);
            this.HistoryTable.TabIndex = 1;
            // 
            // TypeColumnHistory
            // 
            this.TypeColumnHistory.HeaderText = "Type";
            this.TypeColumnHistory.Name = "TypeColumnHistory";
            this.TypeColumnHistory.ReadOnly = true;
            this.TypeColumnHistory.Width = 50;
            // 
            // DocumentNumberColumnHistory
            // 
            this.DocumentNumberColumnHistory.HeaderText = "DocumentNumber";
            this.DocumentNumberColumnHistory.Name = "DocumentNumberColumnHistory";
            this.DocumentNumberColumnHistory.ReadOnly = true;
            // 
            // DocumentNameColumnHistory
            // 
            this.DocumentNameColumnHistory.HeaderText = "DocumentName";
            this.DocumentNameColumnHistory.Name = "DocumentNameColumnHistory";
            this.DocumentNameColumnHistory.ReadOnly = true;
            this.DocumentNameColumnHistory.Width = 300;
            // 
            // DateTimeColumnHistory
            // 
            this.DateTimeColumnHistory.HeaderText = "DateTime";
            this.DateTimeColumnHistory.Name = "DateTimeColumnHistory";
            this.DateTimeColumnHistory.ReadOnly = true;
            this.DateTimeColumnHistory.Width = 200;
            // 
            // RequestsTab
            // 
            this.RequestsTab.Controls.Add(this.RequestTable);
            this.RequestsTab.Location = new System.Drawing.Point(4, 22);
            this.RequestsTab.Name = "RequestsTab";
            this.RequestsTab.Padding = new System.Windows.Forms.Padding(3);
            this.RequestsTab.Size = new System.Drawing.Size(1100, 529);
            this.RequestsTab.TabIndex = 1;
            this.RequestsTab.Text = "RequestsTab";
            this.RequestsTab.UseVisualStyleBackColor = true;
            // 
            // RequestTable
            // 
            this.RequestTable.AllowUserToAddRows = false;
            this.RequestTable.AllowUserToDeleteRows = false;
            this.RequestTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RequestTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NumberColumn,
            this.EmployeeNum,
            this.DocumentNumberColumn,
            this.DocumentNameColumn,
            this.StatusColumn,
            this.DateTimeColumn,
            this.ApproveButton,
            this.DeclineButton,
            this.CheckDSButton});
            this.RequestTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RequestTable.Location = new System.Drawing.Point(3, 3);
            this.RequestTable.Name = "RequestTable";
            this.RequestTable.ReadOnly = true;
            this.RequestTable.Size = new System.Drawing.Size(1094, 523);
            this.RequestTable.TabIndex = 0;
            this.RequestTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.RequestTable_CellContentClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.downloadToolStripMenuItem,
            this.getRequestsToolStripMenuItem,
            this.getDocumentsButtonAdmin});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1108, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // downloadToolStripMenuItem
            // 
            this.downloadToolStripMenuItem.Name = "downloadToolStripMenuItem";
            this.downloadToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.downloadToolStripMenuItem.Text = "Get History";
            this.downloadToolStripMenuItem.Click += new System.EventHandler(this.downloadToolStripMenuItem_Click);
            // 
            // getRequestsToolStripMenuItem
            // 
            this.getRequestsToolStripMenuItem.Name = "getRequestsToolStripMenuItem";
            this.getRequestsToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.getRequestsToolStripMenuItem.Text = "Get Requests";
            this.getRequestsToolStripMenuItem.Click += new System.EventHandler(this.getRequestsToolStripMenuItem_Click);
            // 
            // NumberColumn
            // 
            this.NumberColumn.HeaderText = "Number";
            this.NumberColumn.Name = "NumberColumn";
            this.NumberColumn.ReadOnly = true;
            this.NumberColumn.Width = 50;
            // 
            // EmployeeNum
            // 
            this.EmployeeNum.HeaderText = "EmployeeNum";
            this.EmployeeNum.Name = "EmployeeNum";
            this.EmployeeNum.ReadOnly = true;
            // 
            // DocumentNumberColumn
            // 
            this.DocumentNumberColumn.HeaderText = "DocumentNumber";
            this.DocumentNumberColumn.Name = "DocumentNumberColumn";
            this.DocumentNumberColumn.ReadOnly = true;
            // 
            // DocumentNameColumn
            // 
            this.DocumentNameColumn.HeaderText = "DocumentName";
            this.DocumentNameColumn.Name = "DocumentNameColumn";
            this.DocumentNameColumn.ReadOnly = true;
            this.DocumentNameColumn.Width = 300;
            // 
            // StatusColumn
            // 
            this.StatusColumn.HeaderText = "Status";
            this.StatusColumn.Name = "StatusColumn";
            this.StatusColumn.ReadOnly = true;
            this.StatusColumn.Width = 80;
            // 
            // DateTimeColumn
            // 
            this.DateTimeColumn.HeaderText = "DateTime";
            this.DateTimeColumn.Name = "DateTimeColumn";
            this.DateTimeColumn.ReadOnly = true;
            this.DateTimeColumn.Width = 120;
            // 
            // ApproveButton
            // 
            this.ApproveButton.HeaderText = "ApproveButton";
            this.ApproveButton.Name = "ApproveButton";
            this.ApproveButton.ReadOnly = true;
            this.ApproveButton.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ApproveButton.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ApproveButton.Text = "Approve";
            // 
            // DeclineButton
            // 
            this.DeclineButton.HeaderText = "DeclineButton";
            this.DeclineButton.Name = "DeclineButton";
            this.DeclineButton.ReadOnly = true;
            this.DeclineButton.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeclineButton.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DeclineButton.Text = "Decline";
            // 
            // CheckDSButton
            // 
            this.CheckDSButton.HeaderText = "CheckDSButton";
            this.CheckDSButton.Name = "CheckDSButton";
            this.CheckDSButton.ReadOnly = true;
            this.CheckDSButton.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CheckDSButton.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CheckDSButton.Text = "CheckDS";
            // 
            // DocumentsAdminTab
            // 
            this.DocumentsAdminTab.Controls.Add(this.DocumentTableAdmin);
            this.DocumentsAdminTab.Location = new System.Drawing.Point(4, 22);
            this.DocumentsAdminTab.Name = "DocumentsAdminTab";
            this.DocumentsAdminTab.Padding = new System.Windows.Forms.Padding(3);
            this.DocumentsAdminTab.Size = new System.Drawing.Size(1100, 529);
            this.DocumentsAdminTab.TabIndex = 2;
            this.DocumentsAdminTab.Text = "Documents";
            this.DocumentsAdminTab.UseVisualStyleBackColor = true;
            // 
            // DocumentTableAdmin
            // 
            this.DocumentTableAdmin.AllowUserToAddRows = false;
            this.DocumentTableAdmin.AllowUserToDeleteRows = false;
            this.DocumentTableAdmin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DocumentTableAdmin.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DocumentTableAdminDocNum,
            this.DocumentTableAdminDocName,
            this.DocumentTableAdminDate,
            this.DocumentTableAdminTypeDoc,
            this.DocumentTableAdminAuthorName,
            this.DocumentTableAdminAuthorNameSecond,
            this.DocumentTableAdminCheck});
            this.DocumentTableAdmin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DocumentTableAdmin.Location = new System.Drawing.Point(3, 3);
            this.DocumentTableAdmin.Name = "DocumentTableAdmin";
            this.DocumentTableAdmin.ReadOnly = true;
            this.DocumentTableAdmin.Size = new System.Drawing.Size(1094, 523);
            this.DocumentTableAdmin.TabIndex = 2;
            this.DocumentTableAdmin.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DocumentTableAdmin_CellContentClick);
            // 
            // DocumentTableAdminDocNum
            // 
            this.DocumentTableAdminDocNum.HeaderText = "DocumentNumber";
            this.DocumentTableAdminDocNum.Name = "DocumentTableAdminDocNum";
            this.DocumentTableAdminDocNum.ReadOnly = true;
            // 
            // DocumentTableAdminDocName
            // 
            this.DocumentTableAdminDocName.HeaderText = "DocumentName";
            this.DocumentTableAdminDocName.Name = "DocumentTableAdminDocName";
            this.DocumentTableAdminDocName.ReadOnly = true;
            this.DocumentTableAdminDocName.Width = 300;
            // 
            // DocumentTableAdminDate
            // 
            this.DocumentTableAdminDate.HeaderText = "Data Added";
            this.DocumentTableAdminDate.Name = "DocumentTableAdminDate";
            this.DocumentTableAdminDate.ReadOnly = true;
            this.DocumentTableAdminDate.Width = 130;
            // 
            // DocumentTableAdminTypeDoc
            // 
            this.DocumentTableAdminTypeDoc.HeaderText = "Type";
            this.DocumentTableAdminTypeDoc.Name = "DocumentTableAdminTypeDoc";
            this.DocumentTableAdminTypeDoc.ReadOnly = true;
            this.DocumentTableAdminTypeDoc.Width = 50;
            // 
            // DocumentTableAdminAuthorName
            // 
            this.DocumentTableAdminAuthorName.HeaderText = "Author Name";
            this.DocumentTableAdminAuthorName.Name = "DocumentTableAdminAuthorName";
            this.DocumentTableAdminAuthorName.ReadOnly = true;
            // 
            // DocumentTableAdminAuthorNameSecond
            // 
            this.DocumentTableAdminAuthorNameSecond.HeaderText = "Author Second Name";
            this.DocumentTableAdminAuthorNameSecond.Name = "DocumentTableAdminAuthorNameSecond";
            this.DocumentTableAdminAuthorNameSecond.ReadOnly = true;
            this.DocumentTableAdminAuthorNameSecond.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // DocumentTableAdminCheck
            // 
            this.DocumentTableAdminCheck.HeaderText = "Check Signature";
            this.DocumentTableAdminCheck.Name = "DocumentTableAdminCheck";
            this.DocumentTableAdminCheck.ReadOnly = true;
            this.DocumentTableAdminCheck.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DocumentTableAdminCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DocumentTableAdminCheck.Text = "⊂(▀¯▀⊂)";
            this.DocumentTableAdminCheck.Width = 60;
            // 
            // getDocumentsButtonAdmin
            // 
            this.getDocumentsButtonAdmin.Name = "getDocumentsButtonAdmin";
            this.getDocumentsButtonAdmin.Size = new System.Drawing.Size(101, 20);
            this.getDocumentsButtonAdmin.Text = "Get Documents";
            this.getDocumentsButtonAdmin.Click += new System.EventHandler(this.getDocumentsToolStripMenuItem_Click);
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1108, 579);
            this.Controls.Add(this.Tabs);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "AdminForm";
            this.Text = "AdminForm";
            this.Tabs.ResumeLayout(false);
            this.HistoryTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HistoryTable)).EndInit();
            this.RequestsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RequestTable)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.DocumentsAdminTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DocumentTableAdmin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl Tabs;
        private System.Windows.Forms.TabPage HistoryTab;
        private System.Windows.Forms.TabPage RequestsTab;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem downloadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getRequestsToolStripMenuItem;
        private System.Windows.Forms.DataGridView RequestTable;
        private System.Windows.Forms.DataGridView HistoryTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeColumnHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentNumberColumnHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentNameColumnHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateTimeColumnHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentNumberColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateTimeColumn;
        private System.Windows.Forms.DataGridViewButtonColumn ApproveButton;
        private System.Windows.Forms.DataGridViewButtonColumn DeclineButton;
        private System.Windows.Forms.DataGridViewButtonColumn CheckDSButton;
        private System.Windows.Forms.TabPage DocumentsAdminTab;
        private System.Windows.Forms.DataGridView DocumentTableAdmin;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentTableAdminDocNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentTableAdminDocName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentTableAdminDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentTableAdminTypeDoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentTableAdminAuthorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentTableAdminAuthorNameSecond;
        private System.Windows.Forms.DataGridViewButtonColumn DocumentTableAdminCheck;
        private System.Windows.Forms.ToolStripMenuItem getDocumentsButtonAdmin;
    }
}