﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AisSimpleProcess;

namespace GUI
{
    public partial class IloveAIS : Form
    {
        public IloveAIS()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AdminForm newForm = new AdminForm();
            newForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 0)
            {
                ClientForm newCForm = new ClientForm(Convert.ToInt32(textBox1.Text, 10));
                newCForm.Show();
            }
        }

       
    }
}
