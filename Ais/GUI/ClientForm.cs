﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;
using System.Xml;
using AisSimpleProcess;
using Ais;

namespace GUI
{
    public partial class ClientForm : Form
    {
        private int EmployeeID;
        private ActualInterface Something;

        public ClientForm(int EmployeeID)
        {
            //Tonight I'm going to have myself a real good time
            Something = new WorkClass(new HistoryDatabaseClass(), new DocumentDatabaseClass(),
                new RequestDatabaseClass(), new KeysWorkClass());
            this.EmployeeID = EmployeeID;
            InitializeComponent();
            RequestTableClient.Rows.Clear();
            List<RequestSimpleStruct> NEWIWTD = Something.ReturnRequestsForEmployee(EmployeeID);
            if (NEWIWTD != null)
            {
                foreach (RequestSimpleStruct NEWDesc in NEWIWTD)
                {
                    if (NEWDesc.Status == "Approved")
                        RequestTableClient.Rows.Add(NEWDesc.ReqNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "GETDOCUMENT");
                    else
                        RequestTableClient.Rows.Add(NEWDesc.ReqNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "PLS NO");
                }
            }
        }

        private void RequestTableClient_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                //SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "All files (*.*)|*.*";
                saveFileDialog1.ShowDialog();
                /*
                if (saveFileDialog1.FileName != "")
                {
                    System.IO.FileStream fs =
                       (System.IO.FileStream)saveFileDialog1.OpenFile();
                }
                */
                //Needs tests
                File.WriteAllBytes(saveFileDialog1.FileName, Something.GetDocumentByRequest((long)RequestTableClient.CurrentRow.Cells[0].Value));
                MessageBox.Show(
                        "It somehow works",
                        "File Saved",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information,
                        MessageBoxDefaultButton.Button1,
                        MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        private void DocumentButton_Click(object sender, EventArgs e)
        {
            byte[] KappBytes = File.ReadAllBytes(DocumentFile.Text);
            Employee KeppEmpl = new Employee(EmployeeID, AuthorName.Text, AuthorSecondName.Text);
            try
            {
                if (DocumentType.Text == "Secret")
                    Something.CreateNewSecretDocument(DocumentName.Text, Convert.ToInt32(DocumentNumber.Text, 10), KeppEmpl, KappBytes);
                else
                    Something.CreateNewNormalDocument(DocumentName.Text, Convert.ToInt32(DocumentNumber.Text, 10), KeppEmpl, KappBytes);
            }
            catch(ArgumentException exc)
            {
                MessageBox.Show(
                 exc.Message,
                 "Document Error",
                 MessageBoxButtons.OK,
                 MessageBoxIcon.Information,
                 MessageBoxDefaultButton.Button1,
                 MessageBoxOptions.DefaultDesktopOnly);
                 return;
            }
            MessageBox.Show(
                 "It somehow works",
                 "Document Result",
                 MessageBoxButtons.OK,
                 MessageBoxIcon.Information,
                 MessageBoxDefaultButton.Button1,
                 MessageBoxOptions.DefaultDesktopOnly);
        }

        private void DocFileButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            //byte[] Kapp = File.ReadAllBytes(openFileDialog1.FileName);
            DocumentFile.Text = openFileDialog1.FileName;
        }

        private void RequestButton_Click(object sender, EventArgs e)
        {
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                XmlDocument CloseKDoc = new XmlDocument();
                CloseKDoc.Load(RequestFile.Text);
                string KappString = CloseKDoc.InnerXml;
                Employee KeppEmpl = new Employee(EmployeeID, RequestAuthorN.Text, RequestAuthorSec.Text);
                try
                {
                    Something.CreateRequest(Convert.ToInt32(RequestDocNum.Text, 10), KeppEmpl, KappString);
                }
                catch (ArgumentNullException)
                {
                    MessageBox.Show(
                        "There is no document like this",
                        "Request WRONG",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information,
                        MessageBoxDefaultButton.Button1,
                        MessageBoxOptions.DefaultDesktopOnly);
                    return;
                }
            }
            RequestTableClient.Rows.Clear();
            List<RequestSimpleStruct> NEWIWTD = Something.ReturnRequestsForEmployee(EmployeeID);
            if (NEWIWTD != null)
            {
                foreach (RequestSimpleStruct NEWDesc in NEWIWTD)
                {
                    if (NEWDesc.Status == "Approved")
                        RequestTableClient.Rows.Add(NEWDesc.ReqNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "GETDOCUMENT");
                    else
                        RequestTableClient.Rows.Add(NEWDesc.ReqNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "PLS NO");
                }
            }
            MessageBox.Show(
                        "It somehow works, number is " + NEWIWTD.Last().ReqNumber.ToString(),
                        "Request Result",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information,
                        MessageBoxDefaultButton.Button1,
                        MessageBoxOptions.DefaultDesktopOnly);
        }

        private void CloseKeyButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            RequestFile.Text = openFileDialog1.FileName;
        }

        private void RequestUpdateButton_Click(object sender, EventArgs e)
        {
            RequestTableClient.Rows.Clear();
            List<RequestSimpleStruct> NEWIWTD = Something.ReturnRequestsForEmployee(EmployeeID);
            if (NEWIWTD != null)
            {
                foreach (RequestSimpleStruct NEWDesc in NEWIWTD)
                {
                    if (NEWDesc.Status == "Approved")
                        RequestTableClient.Rows.Add(NEWDesc.ReqNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "GETDOCUMENT");
                    else
                        RequestTableClient.Rows.Add(NEWDesc.ReqNumber, NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.Status, NEWDesc.Date, "PLS NO");
                }
            }
        }

        private void GetDocumentsButton_Click(object sender, EventArgs e)
        {
            DocumentTable.Rows.Clear();
            List<DocumentSimpleStruct> Hello = Something.ReturnSimpleDocumentsAll();
            if (Hello != null)
            {
                foreach (DocumentSimpleStruct NEWDesc in Hello)
                {
                    DocumentTable.Rows.Add(NEWDesc.DocumentNumber, NEWDesc.DocumentName, NEWDesc.AddTime, NEWDesc.Type, NEWDesc.AuthorName, NEWDesc.AuthorSecond);
                }
            }
        }
    }
}
