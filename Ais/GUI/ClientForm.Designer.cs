﻿namespace GUI
{
    partial class ClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.CheckRequestsTab = new System.Windows.Forms.TabPage();
            this.RequestTableClient = new System.Windows.Forms.DataGridView();
            this.NumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GetDocumentButtonColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.AddDocumentTab = new System.Windows.Forms.TabPage();
            this.DocumentType = new System.Windows.Forms.ComboBox();
            this.DocumentTypeRO = new System.Windows.Forms.TextBox();
            this.AuthorSecondNameRO = new System.Windows.Forms.TextBox();
            this.DocumentName = new System.Windows.Forms.TextBox();
            this.DocumentNameRO = new System.Windows.Forms.TextBox();
            this.DocFileButton = new System.Windows.Forms.Button();
            this.DocumentFile = new System.Windows.Forms.TextBox();
            this.DocumentFileRO = new System.Windows.Forms.TextBox();
            this.DocumentNumber = new System.Windows.Forms.TextBox();
            this.DocumentNumberRO = new System.Windows.Forms.TextBox();
            this.AuthorName = new System.Windows.Forms.TextBox();
            this.AuthorSecondName = new System.Windows.Forms.TextBox();
            this.AuthorNameRO = new System.Windows.Forms.TextBox();
            this.DocumentButton = new System.Windows.Forms.Button();
            this.AddRequestTab = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.CloseKeyButton = new System.Windows.Forms.Button();
            this.RequestFile = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.RequestDocNum = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.RequestAuthorN = new System.Windows.Forms.TextBox();
            this.RequestAuthorSec = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.RequestButton = new System.Windows.Forms.Button();
            this.DocumentsTab = new System.Windows.Forms.TabPage();
            this.DocumentTable = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataAddedDoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeDoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorNameDoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorNameSecondDoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.RequestUpdateButton = new System.Windows.Forms.ToolStripMenuItem();
            this.GetDocumentsButton = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.CheckRequestsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RequestTableClient)).BeginInit();
            this.AddDocumentTab.SuspendLayout();
            this.AddRequestTab.SuspendLayout();
            this.DocumentsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentTable)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.CheckRequestsTab);
            this.tabControl1.Controls.Add(this.AddDocumentTab);
            this.tabControl1.Controls.Add(this.AddRequestTab);
            this.tabControl1.Controls.Add(this.DocumentsTab);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(838, 482);
            this.tabControl1.TabIndex = 0;
            // 
            // CheckRequestsTab
            // 
            this.CheckRequestsTab.Controls.Add(this.RequestTableClient);
            this.CheckRequestsTab.Location = new System.Drawing.Point(4, 22);
            this.CheckRequestsTab.Name = "CheckRequestsTab";
            this.CheckRequestsTab.Padding = new System.Windows.Forms.Padding(3);
            this.CheckRequestsTab.Size = new System.Drawing.Size(830, 456);
            this.CheckRequestsTab.TabIndex = 0;
            this.CheckRequestsTab.Text = "CheckRequests";
            this.CheckRequestsTab.UseVisualStyleBackColor = true;
            // 
            // RequestTableClient
            // 
            this.RequestTableClient.AllowUserToAddRows = false;
            this.RequestTableClient.AllowUserToDeleteRows = false;
            this.RequestTableClient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RequestTableClient.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NumberColumn,
            this.DocumentNumberColumn,
            this.DocumentNameColumn,
            this.StatusColumn,
            this.DateTimeColumn,
            this.GetDocumentButtonColumn});
            this.RequestTableClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RequestTableClient.Location = new System.Drawing.Point(3, 3);
            this.RequestTableClient.Name = "RequestTableClient";
            this.RequestTableClient.ReadOnly = true;
            this.RequestTableClient.Size = new System.Drawing.Size(824, 450);
            this.RequestTableClient.TabIndex = 0;
            this.RequestTableClient.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.RequestTableClient_CellContentClick);
            // 
            // NumberColumn
            // 
            this.NumberColumn.HeaderText = "Number";
            this.NumberColumn.Name = "NumberColumn";
            this.NumberColumn.ReadOnly = true;
            this.NumberColumn.Width = 50;
            // 
            // DocumentNumberColumn
            // 
            this.DocumentNumberColumn.HeaderText = "DocumentNumber";
            this.DocumentNumberColumn.Name = "DocumentNumberColumn";
            this.DocumentNumberColumn.ReadOnly = true;
            // 
            // DocumentNameColumn
            // 
            this.DocumentNameColumn.HeaderText = "DocumentName";
            this.DocumentNameColumn.Name = "DocumentNameColumn";
            this.DocumentNameColumn.ReadOnly = true;
            this.DocumentNameColumn.Width = 300;
            // 
            // StatusColumn
            // 
            this.StatusColumn.HeaderText = "Status";
            this.StatusColumn.Name = "StatusColumn";
            this.StatusColumn.ReadOnly = true;
            this.StatusColumn.Width = 50;
            // 
            // DateTimeColumn
            // 
            this.DateTimeColumn.HeaderText = "DateTime";
            this.DateTimeColumn.Name = "DateTimeColumn";
            this.DateTimeColumn.ReadOnly = true;
            this.DateTimeColumn.Width = 130;
            // 
            // GetDocumentButtonColumn
            // 
            this.GetDocumentButtonColumn.HeaderText = "GetDocumentButton";
            this.GetDocumentButtonColumn.Name = "GetDocumentButtonColumn";
            this.GetDocumentButtonColumn.ReadOnly = true;
            this.GetDocumentButtonColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GetDocumentButtonColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.GetDocumentButtonColumn.Width = 120;
            // 
            // AddDocumentTab
            // 
            this.AddDocumentTab.Controls.Add(this.DocumentType);
            this.AddDocumentTab.Controls.Add(this.DocumentTypeRO);
            this.AddDocumentTab.Controls.Add(this.AuthorSecondNameRO);
            this.AddDocumentTab.Controls.Add(this.DocumentName);
            this.AddDocumentTab.Controls.Add(this.DocumentNameRO);
            this.AddDocumentTab.Controls.Add(this.DocFileButton);
            this.AddDocumentTab.Controls.Add(this.DocumentFile);
            this.AddDocumentTab.Controls.Add(this.DocumentFileRO);
            this.AddDocumentTab.Controls.Add(this.DocumentNumber);
            this.AddDocumentTab.Controls.Add(this.DocumentNumberRO);
            this.AddDocumentTab.Controls.Add(this.AuthorName);
            this.AddDocumentTab.Controls.Add(this.AuthorSecondName);
            this.AddDocumentTab.Controls.Add(this.AuthorNameRO);
            this.AddDocumentTab.Controls.Add(this.DocumentButton);
            this.AddDocumentTab.Location = new System.Drawing.Point(4, 22);
            this.AddDocumentTab.Name = "AddDocumentTab";
            this.AddDocumentTab.Padding = new System.Windows.Forms.Padding(3);
            this.AddDocumentTab.Size = new System.Drawing.Size(830, 456);
            this.AddDocumentTab.TabIndex = 1;
            this.AddDocumentTab.Text = "AddDocument";
            this.AddDocumentTab.UseVisualStyleBackColor = true;
            // 
            // DocumentType
            // 
            this.DocumentType.FormattingEnabled = true;
            this.DocumentType.Items.AddRange(new object[] {
            "Normal",
            "Secret"});
            this.DocumentType.Location = new System.Drawing.Point(113, 31);
            this.DocumentType.Name = "DocumentType";
            this.DocumentType.Size = new System.Drawing.Size(83, 21);
            this.DocumentType.TabIndex = 13;
            // 
            // DocumentTypeRO
            // 
            this.DocumentTypeRO.Location = new System.Drawing.Point(113, 6);
            this.DocumentTypeRO.Name = "DocumentTypeRO";
            this.DocumentTypeRO.ReadOnly = true;
            this.DocumentTypeRO.Size = new System.Drawing.Size(83, 20);
            this.DocumentTypeRO.TabIndex = 12;
            this.DocumentTypeRO.Text = "DocumentType";
            // 
            // AuthorSecondNameRO
            // 
            this.AuthorSecondNameRO.Location = new System.Drawing.Point(8, 214);
            this.AuthorSecondNameRO.Name = "AuthorSecondNameRO";
            this.AuthorSecondNameRO.ReadOnly = true;
            this.AuthorSecondNameRO.Size = new System.Drawing.Size(105, 20);
            this.AuthorSecondNameRO.TabIndex = 11;
            this.AuthorSecondNameRO.Text = "AuthorSecondName";
            // 
            // DocumentName
            // 
            this.DocumentName.Location = new System.Drawing.Point(8, 84);
            this.DocumentName.Name = "DocumentName";
            this.DocumentName.Size = new System.Drawing.Size(188, 20);
            this.DocumentName.TabIndex = 10;
            // 
            // DocumentNameRO
            // 
            this.DocumentNameRO.Location = new System.Drawing.Point(8, 58);
            this.DocumentNameRO.Name = "DocumentNameRO";
            this.DocumentNameRO.ReadOnly = true;
            this.DocumentNameRO.Size = new System.Drawing.Size(93, 20);
            this.DocumentNameRO.TabIndex = 9;
            this.DocumentNameRO.Text = "DocumentName";
            // 
            // DocFileButton
            // 
            this.DocFileButton.Location = new System.Drawing.Point(202, 136);
            this.DocFileButton.Name = "DocFileButton";
            this.DocFileButton.Size = new System.Drawing.Size(105, 20);
            this.DocFileButton.TabIndex = 8;
            this.DocFileButton.Text = "Choose Document";
            this.DocFileButton.UseVisualStyleBackColor = true;
            this.DocFileButton.Click += new System.EventHandler(this.DocFileButton_Click);
            // 
            // DocumentFile
            // 
            this.DocumentFile.Location = new System.Drawing.Point(8, 136);
            this.DocumentFile.Name = "DocumentFile";
            this.DocumentFile.Size = new System.Drawing.Size(188, 20);
            this.DocumentFile.TabIndex = 7;
            // 
            // DocumentFileRO
            // 
            this.DocumentFileRO.Location = new System.Drawing.Point(8, 110);
            this.DocumentFileRO.Name = "DocumentFileRO";
            this.DocumentFileRO.ReadOnly = true;
            this.DocumentFileRO.Size = new System.Drawing.Size(81, 20);
            this.DocumentFileRO.TabIndex = 6;
            this.DocumentFileRO.Text = "Document File";
            // 
            // DocumentNumber
            // 
            this.DocumentNumber.Location = new System.Drawing.Point(8, 32);
            this.DocumentNumber.Name = "DocumentNumber";
            this.DocumentNumber.Size = new System.Drawing.Size(93, 20);
            this.DocumentNumber.TabIndex = 5;
            // 
            // DocumentNumberRO
            // 
            this.DocumentNumberRO.Location = new System.Drawing.Point(8, 6);
            this.DocumentNumberRO.Name = "DocumentNumberRO";
            this.DocumentNumberRO.ReadOnly = true;
            this.DocumentNumberRO.Size = new System.Drawing.Size(93, 20);
            this.DocumentNumberRO.TabIndex = 4;
            this.DocumentNumberRO.Text = "DocumentNumber";
            // 
            // AuthorName
            // 
            this.AuthorName.Location = new System.Drawing.Point(8, 188);
            this.AuthorName.Name = "AuthorName";
            this.AuthorName.Size = new System.Drawing.Size(188, 20);
            this.AuthorName.TabIndex = 3;
            // 
            // AuthorSecondName
            // 
            this.AuthorSecondName.Location = new System.Drawing.Point(8, 240);
            this.AuthorSecondName.Name = "AuthorSecondName";
            this.AuthorSecondName.Size = new System.Drawing.Size(188, 20);
            this.AuthorSecondName.TabIndex = 2;
            // 
            // AuthorNameRO
            // 
            this.AuthorNameRO.Location = new System.Drawing.Point(8, 162);
            this.AuthorNameRO.Name = "AuthorNameRO";
            this.AuthorNameRO.ReadOnly = true;
            this.AuthorNameRO.Size = new System.Drawing.Size(70, 20);
            this.AuthorNameRO.TabIndex = 1;
            this.AuthorNameRO.Text = "AuthorName";
            // 
            // DocumentButton
            // 
            this.DocumentButton.Font = new System.Drawing.Font("Old English Text MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DocumentButton.Location = new System.Drawing.Point(8, 266);
            this.DocumentButton.Name = "DocumentButton";
            this.DocumentButton.Size = new System.Drawing.Size(93, 40);
            this.DocumentButton.TabIndex = 0;
            this.DocumentButton.Text = "Submit";
            this.DocumentButton.UseVisualStyleBackColor = true;
            this.DocumentButton.Click += new System.EventHandler(this.DocumentButton_Click);
            // 
            // AddRequestTab
            // 
            this.AddRequestTab.Controls.Add(this.textBox1);
            this.AddRequestTab.Controls.Add(this.CloseKeyButton);
            this.AddRequestTab.Controls.Add(this.RequestFile);
            this.AddRequestTab.Controls.Add(this.textBox3);
            this.AddRequestTab.Controls.Add(this.RequestDocNum);
            this.AddRequestTab.Controls.Add(this.textBox5);
            this.AddRequestTab.Controls.Add(this.RequestAuthorN);
            this.AddRequestTab.Controls.Add(this.RequestAuthorSec);
            this.AddRequestTab.Controls.Add(this.textBox8);
            this.AddRequestTab.Controls.Add(this.RequestButton);
            this.AddRequestTab.Location = new System.Drawing.Point(4, 22);
            this.AddRequestTab.Name = "AddRequestTab";
            this.AddRequestTab.Padding = new System.Windows.Forms.Padding(3);
            this.AddRequestTab.Size = new System.Drawing.Size(830, 456);
            this.AddRequestTab.TabIndex = 2;
            this.AddRequestTab.Text = "AddRequest";
            this.AddRequestTab.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(8, 162);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(105, 20);
            this.textBox1.TabIndex = 21;
            this.textBox1.Text = "AuthorSecondName";
            // 
            // CloseKeyButton
            // 
            this.CloseKeyButton.Location = new System.Drawing.Point(202, 84);
            this.CloseKeyButton.Name = "CloseKeyButton";
            this.CloseKeyButton.Size = new System.Drawing.Size(105, 20);
            this.CloseKeyButton.TabIndex = 20;
            this.CloseKeyButton.Text = "Choose DS File";
            this.CloseKeyButton.UseVisualStyleBackColor = true;
            this.CloseKeyButton.Click += new System.EventHandler(this.CloseKeyButton_Click);
            // 
            // RequestFile
            // 
            this.RequestFile.Location = new System.Drawing.Point(8, 84);
            this.RequestFile.Name = "RequestFile";
            this.RequestFile.Size = new System.Drawing.Size(188, 20);
            this.RequestFile.TabIndex = 19;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(8, 58);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(105, 20);
            this.textBox3.TabIndex = 18;
            this.textBox3.Text = "Digital Signature File";
            // 
            // RequestDocNum
            // 
            this.RequestDocNum.Location = new System.Drawing.Point(8, 32);
            this.RequestDocNum.Name = "RequestDocNum";
            this.RequestDocNum.Size = new System.Drawing.Size(93, 20);
            this.RequestDocNum.TabIndex = 17;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(8, 6);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(93, 20);
            this.textBox5.TabIndex = 16;
            this.textBox5.Text = "DocumentNumber";
            // 
            // RequestAuthorN
            // 
            this.RequestAuthorN.Location = new System.Drawing.Point(8, 136);
            this.RequestAuthorN.Name = "RequestAuthorN";
            this.RequestAuthorN.Size = new System.Drawing.Size(188, 20);
            this.RequestAuthorN.TabIndex = 15;
            // 
            // RequestAuthorSec
            // 
            this.RequestAuthorSec.Location = new System.Drawing.Point(8, 188);
            this.RequestAuthorSec.Name = "RequestAuthorSec";
            this.RequestAuthorSec.Size = new System.Drawing.Size(188, 20);
            this.RequestAuthorSec.TabIndex = 14;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(8, 110);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(70, 20);
            this.textBox8.TabIndex = 13;
            this.textBox8.Text = "AuthorName";
            // 
            // RequestButton
            // 
            this.RequestButton.Font = new System.Drawing.Font("Old English Text MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RequestButton.Location = new System.Drawing.Point(8, 214);
            this.RequestButton.Name = "RequestButton";
            this.RequestButton.Size = new System.Drawing.Size(93, 40);
            this.RequestButton.TabIndex = 12;
            this.RequestButton.Text = "Submit";
            this.RequestButton.UseVisualStyleBackColor = true;
            this.RequestButton.Click += new System.EventHandler(this.RequestButton_Click);
            // 
            // DocumentsTab
            // 
            this.DocumentsTab.Controls.Add(this.DocumentTable);
            this.DocumentsTab.Location = new System.Drawing.Point(4, 22);
            this.DocumentsTab.Name = "DocumentsTab";
            this.DocumentsTab.Padding = new System.Windows.Forms.Padding(3);
            this.DocumentsTab.Size = new System.Drawing.Size(830, 456);
            this.DocumentsTab.TabIndex = 3;
            this.DocumentsTab.Text = "Documents";
            this.DocumentsTab.UseVisualStyleBackColor = true;
            // 
            // DocumentTable
            // 
            this.DocumentTable.AllowUserToAddRows = false;
            this.DocumentTable.AllowUserToDeleteRows = false;
            this.DocumentTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DocumentTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.DataAddedDoc,
            this.TypeDoc,
            this.AuthorNameDoc,
            this.AuthorNameSecondDoc});
            this.DocumentTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DocumentTable.Location = new System.Drawing.Point(3, 3);
            this.DocumentTable.Name = "DocumentTable";
            this.DocumentTable.ReadOnly = true;
            this.DocumentTable.Size = new System.Drawing.Size(824, 450);
            this.DocumentTable.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "DocumentNumber";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "DocumentName";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 300;
            // 
            // DataAddedDoc
            // 
            this.DataAddedDoc.HeaderText = "Data Added";
            this.DataAddedDoc.Name = "DataAddedDoc";
            this.DataAddedDoc.ReadOnly = true;
            this.DataAddedDoc.Width = 130;
            // 
            // TypeDoc
            // 
            this.TypeDoc.HeaderText = "Type";
            this.TypeDoc.Name = "TypeDoc";
            this.TypeDoc.ReadOnly = true;
            this.TypeDoc.Width = 50;
            // 
            // AuthorNameDoc
            // 
            this.AuthorNameDoc.HeaderText = "Author Name";
            this.AuthorNameDoc.Name = "AuthorNameDoc";
            this.AuthorNameDoc.ReadOnly = true;
            // 
            // AuthorNameSecondDoc
            // 
            this.AuthorNameSecondDoc.HeaderText = "Author Second Name";
            this.AuthorNameSecondDoc.Name = "AuthorNameSecondDoc";
            this.AuthorNameSecondDoc.ReadOnly = true;
            this.AuthorNameSecondDoc.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RequestUpdateButton,
            this.GetDocumentsButton});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(838, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // RequestUpdateButton
            // 
            this.RequestUpdateButton.Name = "RequestUpdateButton";
            this.RequestUpdateButton.Size = new System.Drawing.Size(107, 20);
            this.RequestUpdateButton.Text = "Update Requests";
            this.RequestUpdateButton.Click += new System.EventHandler(this.RequestUpdateButton_Click);
            // 
            // GetDocumentsButton
            // 
            this.GetDocumentsButton.Name = "GetDocumentsButton";
            this.GetDocumentsButton.Size = new System.Drawing.Size(122, 20);
            this.GetDocumentsButton.Text = "Get Documents List";
            this.GetDocumentsButton.Click += new System.EventHandler(this.GetDocumentsButton_Click);
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 509);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tabControl1);
            this.Name = "ClientForm";
            this.Text = "ClientForm";
            this.tabControl1.ResumeLayout(false);
            this.CheckRequestsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RequestTableClient)).EndInit();
            this.AddDocumentTab.ResumeLayout(false);
            this.AddDocumentTab.PerformLayout();
            this.AddRequestTab.ResumeLayout(false);
            this.AddRequestTab.PerformLayout();
            this.DocumentsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DocumentTable)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage CheckRequestsTab;
        private System.Windows.Forms.TabPage AddDocumentTab;
        private System.Windows.Forms.TabPage AddRequestTab;
        private System.Windows.Forms.DataGridView RequestTableClient;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentNumberColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateTimeColumn;
        private System.Windows.Forms.DataGridViewButtonColumn GetDocumentButtonColumn;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TextBox DocumentNumberRO;
        private System.Windows.Forms.TextBox AuthorName;
        private System.Windows.Forms.TextBox AuthorSecondName;
        private System.Windows.Forms.TextBox AuthorNameRO;
        private System.Windows.Forms.Button DocumentButton;
        private System.Windows.Forms.TextBox DocumentNumber;
        private System.Windows.Forms.TextBox DocumentFileRO;
        private System.Windows.Forms.Button DocFileButton;
        private System.Windows.Forms.TextBox DocumentFile;
        private System.Windows.Forms.TextBox DocumentNameRO;
        private System.Windows.Forms.TextBox AuthorSecondNameRO;
        private System.Windows.Forms.TextBox DocumentName;
        private System.Windows.Forms.TextBox DocumentTypeRO;
        private System.Windows.Forms.ComboBox DocumentType;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button CloseKeyButton;
        private System.Windows.Forms.TextBox RequestFile;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox RequestDocNum;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox RequestAuthorN;
        private System.Windows.Forms.TextBox RequestAuthorSec;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Button RequestButton;
        private System.Windows.Forms.TabPage DocumentsTab;
        private System.Windows.Forms.DataGridView DocumentTable;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem RequestUpdateButton;
        private System.Windows.Forms.ToolStripMenuItem GetDocumentsButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataAddedDoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeDoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorNameDoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorNameSecondDoc;
    }
}