﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ais
{
    public class History
    {
        protected char HistoryType;
        protected List<HistoryUnit> HistoryInst;                  //Rewrite to list

        public History(char HistoryType)
        {
            if (HistoryType != 'i' && HistoryType != 'o')
                throw new ArgumentException("Nice Type");
            this.HistoryType = HistoryType;
            this.HistoryInst = new List <HistoryUnit>();
        }

        public void AddUnit(HistoryUnit Unit)
        {
            if (Unit == null)
                throw new ArgumentNullException("Nice HistoryUnit");
            HistoryInst.Add(Unit);
        }

        public List<HistoryUnit> GetUnit(DateTime Start, DateTime Finish)
        {
            List<HistoryUnit> Hist = new List<HistoryUnit>();
            foreach (HistoryUnit Desc in HistoryInst)
            {
                if (Desc.GetDate() < Finish && Desc.GetDate() > Start)
                    Hist.Add(Desc);
            }
            if (Hist.Count == 0)
                return null;
            //throw new KeyNotFoundException("Nothin here");
            return Hist;
        }

        public List<HistoryUnit> GetUnit(Document Doc)
        {
            List<HistoryUnit> Hist = new List<HistoryUnit>();
            foreach(HistoryUnit Desc in HistoryInst)
            {
                if (Desc.GetDoc() == Doc)
                    Hist.Add(Desc);
            }
            if (Hist.Count == 0)
                //throw new KeyNotFoundException("Nothin here");
                return null;
            return Hist;
        }

        public List<HistoryUnit> GetAllHistory()
        {
            if (HistoryInst.Count == 0)
                //throw new KeyNotFoundException("Nothin here");
                return null;
            List<HistoryUnit> Hist = new List<HistoryUnit>(HistoryInst);
            return Hist;
        }
    }
}
