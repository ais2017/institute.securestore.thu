﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ais
{
    public class Employee
    {
        public int id;
        public string FirstName;
        public string SecondName;

        public Employee(int id, string FirstName, string SecondName)
        {
            this.id = id;
            this.FirstName = FirstName;
            this.SecondName = SecondName;
        }
    }
}
