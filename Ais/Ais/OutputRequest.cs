﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ais
{
    public enum Stat
    {
        Considered,
        Approved,
        Declined
    }

    public class Request
    {
        public Document Doc;
        private string CloseKey;
        private DateTime DateRequest;
        private Employee Source;
        private Stat Status;

        public Request(Document Doc, string CloseKey, DateTime DateRequest, Employee Source)
        {
            if (Doc == null || Source == null)
            {
                throw new ArgumentNullException("Arguments can't be empty");
            }
            this.Doc = Doc;
            this.CloseKey = CloseKey;
            this.DateRequest = DateRequest;
            this.Source = Source;
            this.Status = Stat.Considered;  //is hates enum without actually pointing on it
        }

        public Request(Document Doc, string CloseKey, DateTime DateRequest, Employee Source, Stat Status)
        {
            if (Doc == null || Source == null)
            {
                throw new ArgumentNullException("Arguments can't be empty");
            }
            this.Doc = Doc;
            this.CloseKey = CloseKey;
            this.DateRequest = DateRequest;
            this.Source = Source;
            this.Status = Status;  //is hates enum without actually pointing on it
        }

        public Document GetDoc() { return this.Doc; }
        public string GetCloseKey() { return this.CloseKey; }
        public DateTime GetDateRequest() { return this.DateRequest; }
        public Employee GetSource() { return this.Source; }
        public Stat GetStatus() { return this.Status; }

        //I have no idea, why i'm doing this. Actually.
        public void Decline()
        {
            if (this.Status != Stat.Considered)
            {
                throw new ArgumentException("Already Checked");
            }
            this.Status = Stat.Declined;
        }
        public void Approve()
        {
            if (this.Status != Stat.Considered)
            {
                throw new ArgumentException("Already Checked");
            }
            this.Status = Stat.Approved;
        }
    }
}
