﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ais
{
    public class HistoryUnit
    {
        private char IOType;
        private DateTime Date;
        private Document Doc;

        public HistoryUnit(char IOType, Document Doc)
        {
            if (Doc == null)
            {
                throw new ArgumentNullException("Nice document, idiot");
            }
            this.IOType = IOType;
            this.Date = DateTime.Now;
            this.Doc = Doc;
        }

        public char GetIOType() { return this.IOType; }
        public DateTime GetDate() { return this.Date; }
        public Document GetDoc() { return this.Doc; }
    }
}
