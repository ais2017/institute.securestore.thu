﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Text;

namespace Ais
{
    abstract public class Document
    {
        protected string _publicKeyXml;
  //      protected string _privateKeyXml;
        //protected RSAParameters _publicKey;
        //protected RSAParameters _privateKey;

        //protected const string ContainerName = "SomeContainer";
        //protected static RSACryptoServiceProvider RsaKey;

        protected string DocumentName;
        protected long DocumentNumber;
        protected string DigitalSignature;
        protected Employee Author;
        protected DateTime DateIncome;
        protected DateTime DateOutcome;
        protected char ConfLevel;
        protected byte[] Data;

        public Document(string DocumentName, long DocumentNumber, Employee Author, string DigitalSignature,
            DateTime DateIncome, DateTime DateOutcome, char ConfLevel, byte[] Data, string _publicKeyXml)
        {
            //data=bytes
            this.DocumentName = DocumentName;
            this.DocumentNumber = DocumentNumber;
            this.DigitalSignature = DigitalSignature;
            this.DateIncome = DateIncome;
            this.DateIncome = DateIncome;
            this.DateOutcome = DateOutcome;
            this.ConfLevel = ConfLevel;
            this.Author = Author;
            this.Data = Data;
            this._publicKeyXml = _publicKeyXml;
        }
/*
        public Document(string DocumentName, long DocumentNumber, Employee Author,
            DateTime DateIncome, DateTime DateOutcome, char ConfLevel, string Data)
        {
            if (DateOutcome > DateTime.Now || DateIncome > DateTime.Now)
            {
                throw new ArgumentOutOfRangeException("You must have right date");
            }
            if (Author == null || Data == null || DocumentName == null)
            {
                throw new ArgumentException("Arguments can't be empty");
            }
            this.DocumentName = DocumentName;
            this.DocumentNumber = DocumentNumber;
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.PersistKeyInCsp = false;
                _publicKey = rsa.ExportParameters(false);
                _privateKey = rsa.ExportParameters(true);
                DigitalSignature = Convert.ToBase64String(rsa.SignData(Encoding.ASCII.GetBytes(Data), new SHA1CryptoServiceProvider()));
            }
            this.Author = Author;
            this.DateIncome = DateIncome;
            this.DateOutcome = DateOutcome;
            this.ConfLevel = ConfLevel;
            this.Data = Encoding.ASCII.GetBytes(Data);
        }
*/
        public Document(string DocumentName, long DocumentNumber, Employee Author,
           DateTime DateIncome, DateTime DateOutcome, char ConfLevel, byte[] Data, string ImOpenKey, string ImCloseKey)
        {
            if (DateOutcome > DateTime.Now || DateIncome > DateTime.Now)
            {
                throw new ArgumentOutOfRangeException("You must have right date");
            }
            if (Author == null || Data == null || DocumentName == null)
            {
                throw new ArgumentException("Arguments can't be empty");
            }
            this.DocumentName = DocumentName;
            this.DocumentNumber = DocumentNumber;
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.PersistKeyInCsp = false;
                rsa.FromXmlString(ImOpenKey);
                rsa.FromXmlString(ImCloseKey);
                //ImportParameters OpenKey, we need Closekey to get Data now
                //New constructor with ready DigitalSignature
                //_publicKey = ImOpenKey;
                _publicKeyXml = ImOpenKey;
         //       _privateKeyXml = ImCloseKey;
                //_privateKey = rsa.ExportParameters(true);

                //Signing by Private Key!
                DigitalSignature = Convert.ToBase64String(rsa.SignData(Data, new SHA1CryptoServiceProvider()));
            }
            this.Author = Author;
            this.DateIncome = DateIncome;
            this.DateOutcome = DateOutcome;
            this.ConfLevel = ConfLevel;
            this.Data = Data;
        }

        public Document(Document First) //Copyconstructor
        {
            this.DocumentName = First.GetDocumentName();
            this.DocumentNumber = First.GetDocumentNumber();
            this.DigitalSignature = First.GetDigitalSignature();
            this.Author = First.GetAuthor();
            this.DateIncome = First.GetDateIncome();
            this.DateOutcome = First.GetDateOutcome();
            this.ConfLevel = First.GetConfLevel();
            this.Data = First.GetData();
        }

        //I can't do get/set inside
        //public void SetConfLevel(char ConfLevel) { this.ConfLevel = ConfLevel; }
        //So old fashion
        public string GetDocumentName() { return this.DocumentName; }
        public long GetDocumentNumber() { return this.DocumentNumber; }
        public string GetDigitalSignature() { return this.DigitalSignature; }
        public Employee GetAuthor() { return this.Author; }
        public DateTime GetDateIncome() { return this.DateIncome; }
        public DateTime GetDateOutcome() { return this.DateOutcome; }
        public char GetConfLevel() { return this.ConfLevel; }

        public void NewRequestedDate() { this.DateOutcome = DateTime.Now; }
        //public RSAParameters GetOpenKey() { return _publicKey; }
        public string GetOpenKeyString() { return _publicKeyXml; }

        //public abstract string GetData();
        public byte[] GetData() { return this.Data; }
        public abstract byte[] GetData(string privateKey);

        //I hope it works, but only in .NET 3.5, cause in 4 it gives you no RSA
        public bool CheckDS(string PublicKey)
        {
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.FromXmlString(PublicKey);
                return rsa.VerifyData(Data, new SHA1CryptoServiceProvider(), Convert.FromBase64String(DigitalSignature));
            }
        }

    }

        public class NormalDocument : Document
        {
        public NormalDocument(string DocumentName, long DocumentNumber, Employee Author, string DigitalSignature,
                DateTime DateIncome, DateTime DateOutcome, char ConfLevel, byte[] Data, string _publicKeyXml): 
                base(DocumentName, DocumentNumber, Author, DigitalSignature, DateIncome, DateOutcome, ConfLevel, Data, _publicKeyXml)
        { }

        public NormalDocument(string DocumentName, long DocumentNumber, Employee Author, DateTime DateIncome, DateTime DateOutcome, char ConfLevel,
                byte[] Data, string ImOpenKey, string ImCloseKey) : base(DocumentName, DocumentNumber, Author, DateIncome, DateOutcome, ConfLevel, Data, ImOpenKey, ImCloseKey)
            { }

            public NormalDocument(Document First) : base(First) { }

            //public override string GetData() { return Encoding.ASCII.GetString(this.Data); } //actually this is bad
            public override byte[] GetData(string privateKey)
            {
                throw new ArgumentException("Normal Document does not need key");
            }
        }

        public class SecretDocument : Document
        {
            public SecretDocument(string DocumentName, long DocumentNumber, Employee Author, string DigitalSignature,
                DateTime DateIncome, DateTime DateOutcome, char ConfLevel, byte[] Data, string _publicKeyXml): 
                base(DocumentName, DocumentNumber, Author, DigitalSignature, DateIncome, DateOutcome, ConfLevel, Data, _publicKeyXml)
        { }

        public SecretDocument(string DocumentName, long DocumentNumber, Employee Author, DateTime DateIncome, DateTime DateOutcome, char ConfLevel,
                byte[] Data, string ImOpenKey, string ImCloseKey) : base(DocumentName, DocumentNumber, Author, DateIncome, DateOutcome, ConfLevel, Data, ImOpenKey, ImCloseKey)
            {
                using (var rsa = new RSACryptoServiceProvider(2048))
                {
                int b = 0;
                    rsa.FromXmlString(ImCloseKey);
                    int blockSize = (rsa.KeySize / 8) - 32;
                    byte[] buffer = new byte[blockSize];
                    byte[] encryptedBuffer = new byte[blockSize];
                    byte[] encryptedBytes = new byte[(Data.Length/blockSize + 1)*(blockSize+32)];
                    for (int i = 0; i < Data.Length; i += blockSize)
                    {
                        
                        if (blockSize + i > Data.Length && ((Data.Length - i) % blockSize != 0))
                        {
                            buffer = new byte[Data.Length - i];
                            blockSize = Data.Length - i;
                        }
                        
                        if (Data.Length < blockSize)
                        {
                            buffer = new byte[Data.Length];
                            blockSize = Data.Length;
                        }
                        Buffer.BlockCopy(Data, i, buffer, 0, blockSize);
                        encryptedBuffer = rsa.Encrypt(buffer, false);
                        encryptedBuffer.CopyTo(encryptedBytes, i+(32*b));
                        b++;
                    }
                    this.Data = encryptedBytes;
                }
                using (var rsa2 = new RSACryptoServiceProvider(2048))
                {
                    rsa2.PersistKeyInCsp = false;
                    rsa2.FromXmlString(ImOpenKey);
                    rsa2.FromXmlString(ImCloseKey);
                    DigitalSignature = Convert.ToBase64String(rsa2.SignData(this.Data, new SHA1CryptoServiceProvider()));
                }

            }

            public SecretDocument(Document First) : base(First) { }

            //public override string GetData() { throw new ArgumentException("Secret Document needs key"); }
            public override byte[] GetData(string privateKey)
            {
                using (var rsa = new RSACryptoServiceProvider(2048))
                {
                    int b = 0;
                    rsa.FromXmlString(privateKey);
                    int blockSize = rsa.KeySize / 8;            //-32???
                    byte[] buffer = new byte[blockSize];
                    byte[] decryptedBuffer = new byte[blockSize];
                    byte[] decryptedBytes = new byte[this.Data.Length];

                    for (int i = 0; i < this.Data.Length; i += blockSize)
                    {
                        if (blockSize + i == this.Data.Length)
                        {
                            //buffer = new byte[this.Data.Length - i];
                            Buffer.BlockCopy(this.Data, i, buffer, 0, blockSize);
                            decryptedBuffer = new byte[rsa.Decrypt(buffer, false).Length];
                            decryptedBuffer = rsa.Decrypt(buffer, false);
                            decryptedBuffer.CopyTo(decryptedBytes, i - (32 * b));
                            buffer = new byte[i - (32 * b) + decryptedBuffer.Length];
                            Buffer.BlockCopy(decryptedBytes, 0, buffer, 0, i - (32 * b));
                            Buffer.BlockCopy(decryptedBuffer, 0, buffer, i - (32 * b), decryptedBuffer.Length);
                            return buffer;
                            //break;
                        }
                        else
                        {
                            Buffer.BlockCopy(this.Data, i, buffer, 0, blockSize);
                            decryptedBuffer = rsa.Decrypt(buffer, false);
                            decryptedBuffer.CopyTo(decryptedBytes, i - (32 * b));
                            b++;
                        }
                        /*
                        if (this.Data.Length < blockSize)
                        {
                            buffer = new byte[this.Data.Length];
                            blockSize = this.Data.Length;
                        }
                        */


                    }
                return decryptedBytes;
                //return rsa.Decrypt(this.Data, false);
                }
            }
        }
}


/*
public void AssignNewKey()
{
    using (var rsa = new RSACryptoServiceProvider(2048))
    {
        rsa.PersistKeyInCsp = false;
        _publicKey = rsa.ExportParameters(false);
        _privateKey = rsa.ExportParameters(true);
    }
    CspParameters cspParams = new CspParameters(1);
    cspParams.KeyContainerName = ContainerName;
    cspParams.Flags = CspProviderFlags.UseMachineKeyStore;
    cspParams.ProviderName = "Microsoft Strong Cryptographic Provider";
    var Kappa = new RSACryptoServiceProvider(cspParams) { PersistKeyInCsp = true };         //and it works!
}
*/
