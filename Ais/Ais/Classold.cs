﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Text;

public class Employee { }

namespace Ais
{
    abstract public class Document
    {
        protected static RSACryptoServiceProvider RsaKey;

        private string DocumentName;
        private long DocumentNumber;
        protected string DigitalSignature;
        private Employee Author;
        private DateTime DateIncome;
        private DateTime DateOutcome;
        private char ConfLevel;
        protected byte[] Data;

        public Document(string DocumentName, long DocumentNumber, string DigitalSignature, Employee Author,
            DateTime DateIncome, DateTime DateOutcome, char ConfLevel, string Data, string CloseKey)
        {
            if (DateOutcome > DateTime.Now || DateIncome > DateTime.Now)
            {
                throw new ArgumentOutOfRangeException("You must have right date");
            }
            if (Author == null || Data == null || DocumentName == null)
            {
                throw new ArgumentException("Arguments can't be empty");
            }
            this.DocumentName = DocumentName;
            this.DocumentNumber = DocumentNumber;
            this.DigitalSignature = DigitalSignature;
            RsaKey = new RSACryptoServiceProvider();
            RsaKey.FromXmlString(CloseKey);
            RSAPKCS1SignatureFormatter RSAFormatter = new RSAPKCS1SignatureFormatter(RsaKey);
            //Set the hash algorithm to SHA1. 
            RSAFormatter.SetHashAlgorithm("SHA1");
            //Create a signature for HashValue and assign it to 
            //SignedHashValue. 
            DigitalSignature = Encoding.ASCII.GetString(RSAFormatter.CreateSignature(Encoding.ASCII.GetBytes(Data)));
            this.Author = Author;
            this.DateIncome = DateIncome;
            this.DateOutcome = DateOutcome;
            this.ConfLevel = ConfLevel;
            this.Data = Encoding.ASCII.GetBytes(Data);
        }

        //I can't do get/set inside
        public void SetConfLevel(char ConfLevel) { this.ConfLevel = ConfLevel; }
        //So old fashion
        public string GetDocumentName() { return this.DocumentName; }
        public long GetDocumentNumber() { return this.DocumentNumber; }
        public string GetDigitalSignature() { return this.DigitalSignature; }
        public Employee GetAuthor() { return this.Author; }
        public DateTime GetDateIncome() { return this.DateIncome; }
        public DateTime GetDateOutcome() { return this.DateOutcome; }

        public string GetData() { return Encoding.ASCII.GetString(this.Data); } //actually this is bad, but kinda ok, better than override

        public string GetData(string Key)
        {
            if (Key == RsaKey.ToXmlString(false))
                return Encoding.ASCII.GetString(RsaKey.Decrypt(this.Data, false));
            else
                throw new ArgumentException("Wrong key");
        }

        //I hope it works, but only in .NET 3.5, cause in 4 it gives you no RSA
        public bool CheckDS(string Key)
        {
            RSACryptoServiceProvider RsaKeyTemp = new RSACryptoServiceProvider();
            RsaKeyTemp.FromXmlString(Key);
            RSAPKCS1SignatureFormatter RSAFormatter = new RSAPKCS1SignatureFormatter(RsaKeyTemp);
            RSAFormatter.SetHashAlgorithm("SHA1");
            if (DigitalSignature == Encoding.ASCII.GetString(RSAFormatter.CreateSignature(this.Data)))
            {
                return true;
            }
            else
                return false;
        }

        //Don't know is it useful, but it is open anyway
        public string GetOpenKey() { return RsaKey.ToXmlString(false); }
    }

    public class NormalDocument : Document
    {
        public NormalDocument(string DocumentName, long DocumentNumber, string DigitalSignature, Employee Author, DateTime DateIncome, DateTime DateOutcome, char ConfLevel,
            string Data, string CloseKey) : base(DocumentName, DocumentNumber, DigitalSignature, Author, DateIncome, DateOutcome, ConfLevel, Data, CloseKey) { }
    }

    public class SecretDocument : Document
    {
        //public override 
        public SecretDocument(string DocumentName, long DocumentNumber, string DigitalSignature, Employee Author, DateTime DateIncome, DateTime DateOutcome, char ConfLevel,
            string Data, string CloseKey) : base(DocumentName, DocumentNumber, DigitalSignature, Author, DateIncome, DateOutcome, ConfLevel, Data, CloseKey)
        {
            //And this is only reason to, well, didn't see it coming
            this.Data = RsaKey.Encrypt(Encoding.ASCII.GetBytes(Data), false);
        }
    }

    public class OutputRequest
    {
        public enum Stat
        {
            Considered,
            Approved,
            Declined
        }

        public Document Doc;
        private string CloseKey;
        private DateTime DateRequest;
        private Employee Source;
        private Stat Status;

        OutputRequest(Document Doc, string CloseKey, DateTime DateRequest, Employee Source)
        {
            this.Doc = Doc;
            this.CloseKey = CloseKey;
            this.DateRequest = DateRequest;
            this.Source = Source;
            this.Status = Stat.Considered;  //is hates enum without actually pointing on it
        }

        public Document GetDoc() { return this.Doc; }
        public DateTime GetDateRequest() { return this.DateRequest; }
        public Employee GetSource() { return this.Source; }
        public Stat GetStatus() { return this.Status; }

        //I have no idea, why
        public void Decline() { this.Status = Stat.Approved; }
        public void Approve() { this.Status = Stat.Declined; }
    }

    public class HistoryUnit
    {
        private char IOType;
        private DateTime Date;
        private Document Doc;

        HistoryUnit(char IOType, Document Doc)
        {
            if (Doc == null)
            {
                throw new ArgumentException("Nice document");
            }
            this.IOType = IOType;
            this.Date = DateTime.Now;
            this.Doc = Doc;
        }

        public char GetIOType() { return this.IOType; }
        public DateTime GetDate() { return this.Date; }
        public Document GetDoc() { return this.Doc; }
    }

    public class History
    {
        internal int length;

        private char HistoryType;
        private HistoryUnit[] HistoryInst;                  //Just some Arrays here

        History(char HistoryType)
        {
            this.HistoryType = HistoryType;
            this.HistoryInst = null;
            length = 0;
        }

        public virtual void AddUnit(HistoryUnit Unit)
        {
            length++;
            Array.Resize(ref HistoryInst, length);
            HistoryInst[length - 1] = Unit;
        }
        public virtual HistoryUnit[] GetUnit(DateTime Start, DateTime Finish)
        {
            HistoryUnit[] HistoryInsttemp = null;
            for (int i = 0; i < length; i++)
            {
                if (HistoryInst[i].GetDate() < Finish && HistoryInst[i].GetDate() > Start)
                Array.Resize(ref HistoryInsttemp, HistoryInsttemp.Length+1);
                HistoryInsttemp[HistoryInsttemp.Length] = HistoryInst[i];
            }
            return HistoryInsttemp;
        }
        public virtual HistoryUnit[] GetUnit(Document Doc)
        {
            HistoryUnit[] HistoryInsttemp = null;
            for (int i = 0; i < length; i++)
            {
                if (HistoryInst[i].GetDoc() == Doc)
                    Array.Resize(ref HistoryInsttemp, HistoryInsttemp.Length + 1);
                HistoryInsttemp[HistoryInsttemp.Length] = HistoryInst[i];
            }
            return HistoryInsttemp;
        }

    }

}