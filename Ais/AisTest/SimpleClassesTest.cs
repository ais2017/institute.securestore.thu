﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ais;
using AisSimpleProcess;
using System.Security.Cryptography;
using System.Xml.Linq;

namespace AisTest
{
    [TestClass]
    public class DocumentTest
    {
        private KeysWorkClass IKeys;

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void DocumentTestConstructorNullTest()
        {
            Employee RandomGuy = null;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), AllowDerivedTypes = true)]
        public void DocumentTestConstructorDateTest()
        {
            Employee RandomGuy = null;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.MaxValue, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
        }

        [TestMethod]
        public void DocumentTestConstructorNormalTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var Document2 = new SecretDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
        }

        [TestMethod]
        public void DocumentTestGetDocumentNameTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            string name = "DocumentName";
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument(name, 1, RandomGuy, DateTime.Now, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            Assert.AreEqual(name, Document.GetDocumentName());
        }

        [TestMethod]
        public void DocumentTestGetDocumentNumberTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            long Number = 10;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", Number, RandomGuy, DateTime.Now, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
        }

        [TestMethod]
        public void DocumentTestGetAndCheckDigitalSignature()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 10, RandomGuy, DateTime.Now, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            //string RSAS = Document.GetDigitalSignature();
            string RSAK = Document.GetOpenKeyString();
            Assert.IsTrue(Document.CheckDS(RSAK));
        }

        [TestMethod]
        public void DocumentTestGetDateIncomeTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime mem = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 10, RandomGuy, mem, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            Assert.AreEqual(mem, Document.GetDateIncome());
        }

        [TestMethod]
        public void DocumentTestGetDateOutcomeTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime mem = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 10, RandomGuy, DateTime.Now, mem, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            Assert.AreEqual(mem, Document.GetDateOutcome());
        }

        [TestMethod]
        public void DocumentTestGetNormalDataTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            byte[] mem = Convert.FromBase64String("SomeData");
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 10, RandomGuy, DateTime.Now, DateTime.Now, '1', mem, IKeys.GetOpenKey(), IKeys.GetCloseKey());
            Assert.AreEqual(mem, Document.GetData());
        }

        [TestMethod]
        public void DocumentTestGetSpecialDataTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            byte[] mem = Convert.FromBase64String("SomeData");
            IKeys = new KeysWorkClass();
            var Document = new SecretDocument("DocumentName", 10, RandomGuy, DateTime.Now, DateTime.Now, '1', mem, IKeys.GetOpenKey(), IKeys.GetCloseKey());
            Assert.AreEqual(mem, Document.GetData(IKeys.GetCloseKey()));
        }

    }

    [TestClass]
    public class HistoryUnitTest
    {
        private KeysWorkClass IKeys;

        [TestMethod]
        public void HistoryUnitTestConstructorNormalTest()
        {
            var RandomGuy = new Employee(1, "FirstName", "Secondname");
            IKeys = new KeysWorkClass();
            var Doc = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var mem = new HistoryUnit('i', Doc);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), AllowDerivedTypes = true)]
        public void HistoryUnitTestConstructorNullTest()
        {
            var RandomGuy = new Employee(1, "FirstName", "Secondname");
            //var Doc = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, '1', "SomeData", "Key");
            var mem = new HistoryUnit('i', null);
        }

        [TestMethod]
        public void HistoryUnitTestGetIOTypeTest()
        {
            var RandomGuy = new Employee(1, "FirstName", "Secondname");
            char memechar = 'i';
            IKeys = new KeysWorkClass();
            var Doc = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var mem = new HistoryUnit(memechar, Doc);
            Assert.AreEqual(memechar, mem.GetIOType());
        }

        [TestMethod]
        public void HistoryUnitTestGetDateTimeTest()
        {
            var RandomGuy = new Employee(1, "FirstName", "Secondname");
            IKeys = new KeysWorkClass();
            var Doc = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            DateTime Date = DateTime.Now;
            var mem = new HistoryUnit('i', Doc);
            Assert.IsTrue(Equals(Date, mem.GetDate()));
        }

        [TestMethod]
        public void HistoryUnitTestGetDocTest()
        {
            var RandomGuy = new Employee(1, "FirstName", "Secondname");
            IKeys = new KeysWorkClass();
            var Doc = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, '1', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var mem = new HistoryUnit('i', Doc);
            Assert.AreEqual(Doc, mem.GetDoc());
        }
    }

    [TestClass]
    public class HistoryTest
    {
        private KeysWorkClass IKeys;

        [TestMethod]
        public void HistoryTestConstructorTest()
        {
            var hehe = new History('i');
            var meme = new History('o');
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void HistoryTestConstructorWrongTest()
        {
            var hehe = new History('l');
            var meme = new History('1');
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), AllowDerivedTypes = true)]
        public void HistoryTestAddNullTest()
        {
            var hehe = new History('i');
            hehe.AddUnit(null);
        }

        [TestMethod]
        public void HistoryTestAddNormalTest()
        {
            var RandomGuy = new Employee(1, "FirstName", "Secondname");
            IKeys = new KeysWorkClass();
            var Doc = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var mem = new HistoryUnit('i', Doc);
            var hehe = new History('i');
            hehe.AddUnit(mem);
        }

        [TestMethod]
        public void HistoryTestGetUnitDocTest()
        {
            //placeholder
        }

        [TestMethod]
        public void HistoryTestGetUnitTimeTest()
        {
            //placeholder
        }

        [TestMethod]
        public void HistoryTestGetUnitAllTest()
        {
            //placeholder
        }
    }

    [TestClass]
    public class RequestTest
    {
        private KeysWorkClass IKeys;

        [TestMethod]
        public void OutputrequestTestConsrtuctorTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var hehe = new Request(Document, "CloseKey", DateTime.Now, RandomGuy);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), AllowDerivedTypes = true)]
        public void OutputrequestTestNullTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(null, "CloseKey", DateTime.Now, RandomGuy);
            var hehe = new Request(Document, "CloseKey", DateTime.Now, null);
        }

        [TestMethod]
        public void OutputrequestGetDocTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(Document, "CloseKey", DateTime.Now, RandomGuy);
            Assert.AreEqual(meme.GetDoc(), Document);
        }

        [TestMethod]
        public void OutputrequestGetDateTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime Date = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(Document, "CloseKey", Date, RandomGuy);
            Assert.AreEqual(meme.GetDateRequest(), Date);
        }

        [TestMethod]
        public void OutputrequestGetSourceTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime Date = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, Date, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(Document, "CloseKey", DateTime.Now, RandomGuy);
            Assert.AreEqual(meme.GetSource(), RandomGuy);
        }

        [TestMethod]
        public void OutputrequestGetGetNStatusTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime Date = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, Date, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(Document, "CloseKey", DateTime.Now, RandomGuy);
            Assert.AreEqual(meme.GetStatus(), Stat.Considered);
        }

        [TestMethod]
        public void OutputrequestGetGetSStatusTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime Date = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new SecretDocument("DocumentName", 1, RandomGuy, Date, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(Document, "CloseKey", DateTime.Now, RandomGuy);
            Assert.AreEqual(meme.GetStatus(), Stat.Considered);
        }

        [TestMethod]
        public void OutputrequestDeclineNormalTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime Date = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, Date, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(Document, "CloseKey", DateTime.Now, RandomGuy);
            meme.Decline();
            Assert.AreEqual(meme.GetStatus(), Stat.Declined);
        }

        [TestMethod]
        public void OutputrequestApproveNormalTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime Date = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, Date, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(Document, "CloseKey", DateTime.Now, RandomGuy);
            meme.Approve();
            Assert.AreEqual(meme.GetStatus(), Stat.Approved);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void OutputrequestApproveAfterApproveTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime Date = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, Date, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(Document, "CloseKey", DateTime.Now, RandomGuy);
            meme.Approve();
            meme.Approve();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void OutputrequestApproveAfterDeclineTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime Date = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, Date, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(Document, "CloseKey", DateTime.Now, RandomGuy);
            meme.Approve();
            meme.Decline();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void OutputrequestDeclineAfterApproveTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime Date = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, Date, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(Document, "CloseKey", DateTime.Now, RandomGuy);
            meme.Decline();
            meme.Approve();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void OutputrequestDeclineAfterDeclineTest()
        {
            Employee RandomGuy = new Employee(1, "FirstName", "Secondname");
            DateTime Date = DateTime.Now;
            IKeys = new KeysWorkClass();
            var Document = new NormalDocument("DocumentName", 1, RandomGuy, Date, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKeys.GetOpenKey(), IKeys.GetCloseKey());
            var meme = new Request(Document, "CloseKey", DateTime.Now, RandomGuy);
            meme.Decline();
            meme.Decline();
        }

    }

}

/*
    [TestClass]
    public class ProcessTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ProcessTestConstructorNormalNullTest()
        {
            Employee RandomGuy = null;
            var InputJournal = new History('i');
            HistoryWorkClass.CreateNewNormalDocument("DocumentName", 1, RandomGuy, "SomeData", "Key");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ProcessTestConstructorSecretNullTest()
        {
            Employee RandomGuy = null;
            var InputJournal = new History('i');
            MainWorkClass.CreateNewSecretDocument("DocumentName", 1, RandomGuy, "SomeData", "Key");
        }

        [TestMethod]
        public void ProcessTestConstructorNormalCoolTest()
        {
            var RandomGuy = new Employee();
            var InputJournal = new History('i');
            MainWorkClass.CreateNewNormalDocument("DocumentName", 1, RandomGuy, "SomeData", "Key");
        }

        [TestMethod]
        public void ProcessTestConstructorSecretCoolTest()
        {
            var RandomGuy = new Employee();
            var InputJournal = new History('i');
            MainWorkClass.CreateNewSecretDocument("DocumentName", 1, RandomGuy, "SomeData", "Key");
        }

        [TestMethod]
        public void ProcessTestConstructorOrderCoolTest()
        {
            var RandomGuy = new Employee();
            var RJournal = new History('o');
            var Doc = new NormalDocument("DocumentName", 1, RandomGuy, DateTime.Now, DateTime.Now, '1', "SomeData", "34524785452454745242");
            AddDocument.CreateRequest(Doc, RandomGuy, "RandomKeString", RJournal);
        }
    }
}
*/