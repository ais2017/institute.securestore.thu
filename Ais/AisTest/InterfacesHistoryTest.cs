﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ais;
using AisSimpleProcess;

namespace AisTest
{
    [TestClass]
    public class InterfacesHistoryTest
    {
        [TestMethod]
        public void InterfacesHistoryTestEmptyTest()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            HistoryInterface test = new HistoryWorkClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            Assert.IsNull(test.ReturnHistoryAll());
        }
        [TestMethod]

        public void InterfacesHistoryTestGetAllTest()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            HistoryInterface test = new HistoryWorkClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            HistoryUnit Kappa = new HistoryUnit('i', Somedoc);
            HistorySimpleStruct Struct = new HistorySimpleStruct( 'i', DateTime.Now, "Secret", 1 );
            test.AddUnit(Kappa);
            Assert.AreEqual(1,test.ReturnHistoryAll().Count);
            Assert.AreEqual(Struct.DocumentName, test.ReturnHistoryAll().Find(r => r.DocumentNumber == 1).DocumentName);
            Assert.AreEqual(Struct.DocumentNumber, test.ReturnHistoryAll().Find(r => r.DocumentName == "Secret").DocumentNumber);
        }

        public void InterfacesHistoryTestGetByDataTest()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            HistoryInterface test = new HistoryWorkClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            Document Somedoc2 = new SecretDocument("Secret1", 2, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            HistoryUnit Kappa = new HistoryUnit('i', Somedoc);
            HistoryUnit KappaPride = new HistoryUnit('i', Somedoc2);
            HistorySimpleStruct Struct = new HistorySimpleStruct('i', DateTime.Now, "Secret", 1);
            test.AddUnit(Kappa);
            test.AddUnit(KappaPride);
            DateTime From1 = new DateTime(2017,10,10);
            DateTime From2 = new DateTime(2012, 10, 10);
            DateTime To1 = new DateTime(2018, 10, 10);
            DateTime To2 = new DateTime(2013, 10, 10);
            Assert.AreEqual(1, test.ReturnHistoryByData(From1, To1).Count);
            Assert.AreEqual(2, test.ReturnHistoryByData(From2, To1).Count);
            Assert.IsNull(test.ReturnHistoryByData(From2, To2));
            Assert.AreEqual(Struct.DocumentName, test.ReturnHistoryByData(From1, To1).Find(r => r.DocumentNumber == 1).DocumentName);
            Assert.AreEqual(Struct.DocumentNumber, test.ReturnHistoryByData(From1, To1).Find(r => r.DocumentName == "Secret").DocumentNumber);
        }

    }
}
