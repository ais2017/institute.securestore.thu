﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ais;
using AisSimpleProcess;

namespace AisTest
{
    [TestClass]
    public class InterfacesDocumentTest
    {
        [TestMethod]
        public void InterfacesDocumentTestGetByNumber()
        {
            Employee Empl = new Employee(1,"FirstName","Secondname");
            KeysInterface IKey = new KeysWorkClass();
            DocumentInterface test = new DocumentWorkClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            test.AddDocument(Somedoc);
            Assert.IsNull(test.GetDocByNumber(2));
            Assert.AreEqual(Somedoc, test.GetDocByNumber(1));
        }
        /*
        [TestMethod]
        public void InterfacesDocumentDBTestAdd()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            DocumentInterface test = new DocumentDatabaseClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', "Somedata", IKey.GetOpenKey(), IKey.GetCloseKey());
            test.AddDocument(Somedoc);
        }
        */
        [TestMethod]
        public void InterfacesDocumentDBTestGetByNumber()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            DocumentInterface test = new DocumentDatabaseClass();
            Document Somedoc2 = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            test.AddDocument(Somedoc2);
            Document Somedoc = test.GetDocByNumber(1);
            Assert.AreEqual(Somedoc.GetDocumentName(), Somedoc2.GetDocumentName());
            Assert.AreEqual(Somedoc.GetDocumentName(), Somedoc2.GetDocumentName());
            Assert.AreEqual(Somedoc.GetConfLevel(), Somedoc2.GetConfLevel());
            Assert.AreEqual(Somedoc.GetData(), Somedoc2.GetData());
            Assert.AreEqual(Somedoc.GetDocumentName(), Somedoc2.GetDocumentName());
        }

        [TestMethod]
        public void InterfacesDocumentDBTestGetAll()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            DocumentInterface test = new DocumentDatabaseClass();
            Document Somedoc2 = new SecretDocument("Secret", 2, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            test.AddDocument(Somedoc2);
            List<Document> Somedoc = test.GetAll();
            Assert.AreEqual(Somedoc.Count, 2);
        }


        /*
        [TestMethod]
        public void InterfacesDocumentTestChackByDoc()
        {
            Employee Empl = new Employee();
            KeysInterface IKey = new KeysWorkClass();
            DocumentInterface test = new DocumentWorkClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', "Somedata", IKey.GetOpenKey(), IKey.GetCloseKey());
            Document Somedoc2 = new SecretDocument("Secret2", 2, Empl, DateTime.Now, DateTime.Now, 'C', "Somedata", IKey.GetOpenKey(), IKey.GetCloseKey());
            test.AddDocument(Somedoc);
            Assert.IsTrue(test.CheckDocByDoc(Somedoc));
            Assert.IsTrue(!test.CheckDocByDoc(Somedoc2));
        }
        */

    }
    
}
