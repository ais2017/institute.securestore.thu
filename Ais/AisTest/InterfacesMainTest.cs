﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Text;
using Ais;
using AisSimpleProcess;

namespace AisTest
{
    [TestClass]
    public class InterfacesMainTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void InterfacesMainTestAddsDocuments()
        {
            Employee Shrek = new Employee(1, "FirstName", "Secondname");
            ActualInterface Allstars = new WorkClass(new HistoryDatabaseClass(), new DocumentDatabaseClass(),
                new RequestDatabaseClass(), new KeysWorkClass());
            Allstars.CreateNewNormalDocument("Once told me", 1, Shrek, Encoding.UTF8.GetBytes("Something"));
            Allstars.CreateNewSecretDocument("Once told me2", 1, Shrek, Encoding.UTF8.GetBytes("Something"));
        }

        [TestMethod]
        public void InterfacesMainTestCreateRequestOK()
        {
            Employee Shrek = new Employee(1, "FirstName", "Secondname");
            ActualInterface Allstars = new WorkClass(new HistoryDatabaseClass(), new DocumentDatabaseClass(),
                new RequestDatabaseClass(), new KeysWorkClass());
            Allstars.CreateNewNormalDocument("Once told me", 1, Shrek, Encoding.UTF8.GetBytes("4HEad"));
            Allstars.CreateRequest(1, Shrek, Allstars.GetCloseKey());
            Allstars.ReturnHistoryAll();
            Allstars.ReturnRequests();
        }
        [TestMethod]
        public void Kapp()
        {
            ActualInterface Allstars = new WorkClass(new HistoryDatabaseClass(), new DocumentDatabaseClass(),
                   new RequestDatabaseClass(), new KeysWorkClass());
            Assert.AreEqual(1, Allstars.ReturnRequests().Count);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void InterfacesMainTestCreateRequestFail()
        {
            Employee Shrek = new Employee(1, "FirstName", "Secondname");
            ActualInterface Allstars = new WorkClass(new HistoryWorkClass(), new DocumentWorkClass(), new RequestWorkClass(), new KeysWorkClass());
            Allstars.CreateNewNormalDocument("Once told me", 1, Shrek, Encoding.UTF8.GetBytes("4Headest"));
            Allstars.CreateRequest(2, Shrek, Allstars.GetCloseKey());
        }



        /*
        [TestMethod]
        public void InterfacesMainTestGetNormalDataByRequest()
        {
            Employee Shrek = new Employee(1, "FirstName", "Secondname");
            ActualInterface Allstars = new WorkClass(new HistoryWorkClass(), new DocumentWorkClass(), new RequestWorkClass(), new KeysWorkClass());
            Allstars.CreateNewNormalDocument("Once told me", 1, Shrek, "The world is gonna roll me");
            long FU = Allstars.CreateRequest(1, Shrek, Allstars.GetCloseKey());
            Assert.AreEqual(Allstars.GetDocumentByRequest(FU), "The world is gonna roll me");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void InterfacesMainTestGetSecretDataByRequestF()
        {
            Employee Shrek = new Employee(1, "FirstName", "Secondname");
            ActualInterface Allstars = new WorkClass(new HistoryWorkClass(), new DocumentWorkClass(), new RequestWorkClass(), new KeysWorkClass());
            Allstars.CreateNewSecretDocument("Once told me", 1, Shrek, "The world is gonna roll me");
            long FU = Allstars.CreateRequest(1, Shrek, Allstars.GetCloseKey());
            Assert.AreEqual(Allstars.GetDocumentByRequest(FU), "The world is gonna roll me");
        }

        [TestMethod]
        public void InterfacesMainTestGetSecretDataByRequestK()
        {
            Employee Shrek = new Employee(1, "FirstName", "Secondname");
            ActualInterface Allstars = new WorkClass(new HistoryWorkClass(), new DocumentWorkClass(), new RequestWorkClass(), new KeysWorkClass());
            Allstars.CreateNewSecretDocument("Once told me", 1, Shrek, "The world is gonna roll me");
            long FU = Allstars.CreateRequest(1, Shrek, Allstars.GetCloseKey());
            Allstars.ApproveRequest(FU);
            Assert.AreEqual(Allstars.GetDocumentByRequest(FU), "The world is gonna roll me");
        }

        [TestMethod]
        public void InterfacesMainTestCheckRequestSign()
        {
            Employee Shrek = new Employee(1, "FirstName", "Secondname");
            ActualInterface Allstars = new WorkClass(new HistoryWorkClass(), new DocumentWorkClass(), new RequestWorkClass(), new KeysWorkClass());
            Allstars.CreateNewNormalDocument("Once told me", 1, Shrek, "The world is gonna roll me");
            long FU = Allstars.CreateRequest(1, Shrek, Allstars.GetCloseKey());
            Assert.IsTrue(Allstars.CheckRequestSign(FU));

            var rsa = new RSACryptoServiceProvider(2048);
            long FUToo = Allstars.CreateRequest(1, Shrek, rsa.ToXmlString(true));
            Assert.IsFalse(Allstars.CheckRequestSign(FUToo));
        }
        */


    }
}
