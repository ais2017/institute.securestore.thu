﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ais;
using AisSimpleProcess;

namespace AisTest
{
    [TestClass]
    public class InterfacesRequestTest
    {

        [TestMethod]
        public void InterfacesRequestTestAddRequest()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            RequestWorkInterface test = new RequestWorkClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            Request Rec = new Request(Somedoc, IKey.GetCloseKey(), DateTime.Now, Empl);
            test.AddRequest(Rec);
            Request temp = test.GetRequestByNumber(1);
            Assert.AreEqual(temp, Rec);
        }

        [TestMethod]
        public void InterfacesRequestTestCheckRequestByNumber()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            RequestWorkInterface test = new RequestWorkClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            Request Rec = new Request(Somedoc, IKey.GetCloseKey(), DateTime.Now, Empl);
            test.AddRequest(Rec);
            Assert.IsTrue(test.CheckRequestByNumber(1));
        }

        [TestMethod]
        public void InterfacesRequestTestApproveRequestOk()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            RequestWorkInterface test = new RequestWorkClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            Request Rec = new Request(Somedoc, IKey.GetCloseKey(), DateTime.Now, Empl);
            test.AddRequest(Rec);
            test.ApproveRequest(1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void InterfacesRequestTestApproveRequestNoRequest()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            RequestWorkInterface test = new RequestWorkClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            Request Rec = new Request(Somedoc, IKey.GetCloseKey(), DateTime.Now, Empl);
            test.AddRequest(Rec);
            test.ApproveRequest(2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void InterfacesRequestTestApproveRequestAccepted()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            RequestWorkInterface test = new RequestWorkClass();
            Document Somedoc = new NormalDocument("Normal", 1, Empl, DateTime.Now, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            Request Rec = new Request(Somedoc, IKey.GetCloseKey(), DateTime.Now, Empl);
            test.AddRequest(Rec);
            test.ApproveRequest(1);
            test.ApproveRequest(1);
        }

        [TestMethod]
        public void InterfacesRequestTestDeclineRequestOk()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            RequestWorkInterface test = new RequestWorkClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            Request Rec = new Request(Somedoc, IKey.GetCloseKey(), DateTime.Now, Empl);
            test.AddRequest(Rec);
            test.DeclineRequest(1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void InterfacesRequestTestDeclineRequestNoRequest()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            RequestWorkInterface test = new RequestWorkClass();
            Document Somedoc = new SecretDocument("Secret", 1, Empl, DateTime.Now, DateTime.Now, 'C', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            Request Rec = new Request(Somedoc, IKey.GetCloseKey(), DateTime.Now, Empl);
            test.AddRequest(Rec);
            test.DeclineRequest(2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void InterfacesRequestTestDeclineRequestAccepted()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            RequestWorkInterface test = new RequestWorkClass();
            Document Somedoc = new NormalDocument("Normal", 1, Empl, DateTime.Now, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            Request Rec = new Request(Somedoc, IKey.GetCloseKey(), DateTime.Now, Empl);
            test.AddRequest(Rec);
            test.DeclineRequest(1);
            test.DeclineRequest(1);
        }

        [TestMethod]
        public void InterfacesRequestTestDB()
        {
            Employee Empl = new Employee(1, "FirstName", "Secondname");
            KeysInterface IKey = new KeysWorkClass();
            RequestWorkInterface test = new RequestDatabaseClass();
            Document Somedoc = new NormalDocument("Normal", 1, Empl, DateTime.Now, DateTime.Now, 'N', Convert.FromBase64String("SomeData"), IKey.GetOpenKey(), IKey.GetCloseKey());
            Request Rec = new Request(Somedoc, IKey.GetCloseKey(), DateTime.Now, Empl);
            test.AddRequest(Rec);
        }

        [TestMethod]
        public void InterfacesRequestTestDBSelectNum()
        {
            RequestWorkInterface test = new RequestDatabaseClass();
            Request Rec = test.GetRequestByNumber(2);
            Assert.AreEqual(Rec.GetSource().FirstName, "FirstName");
        }

        [TestMethod]
        public void InterfacesRequestTestDBSelectAll()
        {
            RequestWorkInterface test = new RequestDatabaseClass();
            List<ReqStruct> Rec = test.GetAll();
            Assert.AreEqual(Rec.Count, 1);
        }


    }
}
