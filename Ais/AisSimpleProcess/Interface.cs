﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ais;

namespace AisSimpleProcess
{

    //for history return only
    public struct HistorySimpleStruct
    {
        public char type;
        public DateTime Date;
        public string DocumentName;
        public long DocumentNumber;
        public HistorySimpleStruct(char type, DateTime Date, string DocumentName, long DocumentNumber)
        { this.type = type; this.Date = Date; this.DocumentName = DocumentName; this.DocumentNumber = DocumentNumber; }
    }

    public struct RequestSimpleStruct
    {
        public long ReqNumber;
        public long EmployeeNumber;
        public DateTime Date;
        public long DocumentNumber;
        public string DocumentName;
        public string Status;

        public RequestSimpleStruct(ReqStruct Kapp)
        {
            ReqNumber = Kapp.ReqNumber;
            EmployeeNumber = Kapp.ReqLink.GetSource().id;
            Date = Kapp.ReqLink.GetDateRequest();
            DocumentNumber = Kapp.ReqLink.GetDoc().GetDocumentNumber();
            DocumentName = Kapp.ReqLink.GetDoc().GetDocumentName();
            Status = Kapp.ReqLink.GetStatus().ToString();
            //MAYBE it won't work, who cares
        }
    }

    public struct DocumentSimpleStruct
    {
        public long DocumentNumber;
        public string DocumentName;
        public DateTime AddTime;
        public string Type;
        public string AuthorName;
        public string AuthorSecond;

        public DocumentSimpleStruct(Document Kapp)
        {
            DocumentNumber = Kapp.GetDocumentNumber();
            DocumentName = Kapp.GetDocumentName();
            AddTime = Kapp.GetDateIncome();
            Type = Kapp.GetConfLevel().ToString();
            AuthorName = Kapp.GetAuthor().FirstName;
            AuthorSecond = Kapp.GetAuthor().FirstName;
        }
    }




    // I have 0 idea why I "need" it for this project
    public interface ActualInterface
    {
        void CreateNewNormalDocument(string DocumentName, long DocumentNumber, Employee Author,
                byte[] Data);
        void CreateNewSecretDocument(string DocumentName, long DocumentNumber, Employee Author,
                byte[] Data);
        List<HistorySimpleStruct> ReturnHistoryAll();
        List<HistorySimpleStruct> ReturnHistoryByData(DateTime Start, DateTime Finish);
        void CreateRequest(long number, Employee Source, string Key);
        bool CheckRequestSign(long Ident);

        bool CheckLocalSign(long Ident);

        void ApproveRequest(long Ident);
        void DeclineRequest(long Ident);

        List<DocumentSimpleStruct> ReturnSimpleDocumentsAll();

        List<RequestSimpleStruct> ReturnRequestsForEmployee(int ID);
        List<RequestSimpleStruct> ReturnRequests();
        byte[] GetDocumentByRequest(long Ident);
        //Actually for tests!
        string GetOpenKey();
        string GetCloseKey();
    }
}
