﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ais;

namespace AisSimpleProcess
{
    public struct ReqStruct
    {
        public long ReqNumber;
        public Request ReqLink;

        public ReqStruct(long ReqNumber, Request ReqLink)
        { this.ReqNumber = ReqNumber; this.ReqLink = ReqLink; }
    }

    public interface RequestWorkInterface
    {
        List<ReqStruct> GetAll();
        void AddRequest(Request meme);
        Request GetRequestByNumber(long num);
        List<ReqStruct> GetRequestsBySource(long num);
        bool CheckRequestByNumber(long num);
        void ApproveRequest(long num);
        void DeclineRequest(long num);
    }

    public class RequestWorkClass: RequestWorkInterface
    {
        private List<ReqStruct> RequestList;

        public RequestWorkClass() { RequestList = new List<ReqStruct>(); }

        public List<ReqStruct> GetAll()
        {
            List<ReqStruct> temp = new List<ReqStruct>(RequestList);
            return temp;
        }

        public void AddRequest(Request meme)
        {
            if (meme == null)
                throw new ArgumentNullException("Nice Document");
            //long num = meme.ToString().GetHashCode();
            long num = 1;
            while (RequestList.Exists(r => r.ReqNumber == num))
                num++;
            ReqStruct Kappa = new ReqStruct(num, meme);
            RequestList.Add(Kappa);
        }

        public Request GetRequestByNumber(long num)
        {
            if (!RequestList.Exists(r => r.ReqNumber == num))
                return null;
            var ret = RequestList.FirstOrDefault(r => r.ReqNumber == num); 
            return ret.ReqLink;
        }

        public List<ReqStruct> GetRequestsBySource(long num)
        {
            List < ReqStruct > ret = new List<ReqStruct>();
            foreach (ReqStruct Desc in RequestList)
            {

                ReqStruct memes = new ReqStruct(Desc.ReqNumber, Desc.ReqLink);
                if (memes.ReqLink.GetSource().id == num)
                { ret.Add(memes); }
            }
            if (ret.Count() == 0)
                return null;
            else
                return ret;
        }

        public bool CheckRequestByNumber(long num)
        {
            var ret = RequestList.Exists(r => r.ReqNumber == num);
            return ret;
        }

        public void ApproveRequest(long num)
        {
            if (RequestList.Exists(r => r.ReqNumber == num))
            {
                var ret = RequestList.FirstOrDefault(r => r.ReqNumber == num);
                ret.ReqLink.Approve();
            }
            else
                throw new ArgumentException("Nice Request");
        }

        public void DeclineRequest(long num)
        {
            if (RequestList.Exists(r => r.ReqNumber == num))
            {
                var ret = RequestList.FirstOrDefault(r => r.ReqNumber == num);
                ret.ReqLink.Decline();
            }
            else
                throw new ArgumentException("Nice Request");
        }
    }
}
