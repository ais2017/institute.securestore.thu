﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Text;
using System.Xml;

namespace AisSimpleProcess
{
    public interface KeysInterface
    {
        string GetOpenKey();
        string GetCloseKey();
    }

    public class KeysWorkClass: KeysInterface
    {
        protected string publicKey;
        protected string privateKey;

        public KeysWorkClass()
        {/*
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                publicKey = rsa.ToXmlString(false);
                privateKey = rsa.ToXmlString(true);
            }
          */
            XmlDocument OpenKDoc = new XmlDocument();
                OpenKDoc.Load("D://OpenKDoc.xml");
                this.publicKey = OpenKDoc.InnerXml;
                XmlDocument CloseKDoc = new XmlDocument();
                CloseKDoc.Load("D://CloseKDoc.xml");
                this.privateKey = CloseKDoc.InnerXml;       //OpenKDoc lol 30 mins
               
         /*
          * Keys generation
         using (var rsa = new RSACryptoServiceProvider(2048))
         {
             rsa.PersistKeyInCsp = false;
             publicKey = rsa.ToXmlString(false);
             privateKey = rsa.ToXmlString(true);
             XmlDocument OpenKDoc = new XmlDocument();
             OpenKDoc.LoadXml(publicKey);
             OpenKDoc.Save("D://OpenKDoc.xml");
             XmlDocument CloseKDoc = new XmlDocument();
             CloseKDoc.LoadXml(privateKey);
             CloseKDoc.Save("D://CloseKDoc.xml");
         }
         */
        }

        public KeysWorkClass(string publicKey, string privateKey)
        {
            this.publicKey = publicKey;
            this.privateKey = privateKey;
        }

        public string GetOpenKey() { return publicKey; }

        public string GetCloseKey() { return privateKey; }

    }

}
