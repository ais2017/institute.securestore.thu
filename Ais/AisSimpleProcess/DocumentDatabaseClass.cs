﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Ais;


namespace AisSimpleProcess
{
    public class DocumentDatabaseClass : DocumentInterface
    {
        private SqlConnection Connection;

        public DocumentDatabaseClass()
        {
            //MAGI-31-MAIN
            Connection = new SqlConnection("Server = localhost; Database = AIS; Integrated Security = true; MultipleActiveResultSets=True");
        }

        public List<Document> GetAll()
        {
            Connection.Open();
            List<Document> ret = new List<Document>();
            SqlCommand Command = new SqlCommand("SELECT * FROM DOCUMENT", Connection);
            SqlDataReader Reader = Command.ExecuteReader();
            char[] buffer = new char[1];
            Document Doc = null;
            while (Reader.Read())
            {
                Reader.GetChars(8, 0, buffer, 0, 1);
                Employee Kappa = new Employee(Reader.GetInt32(3), Reader.GetString(4), Reader.GetString(5));
                if (buffer[0] == 'N')
                {
                    Doc = new NormalDocument(Reader.GetString(1), Reader.GetInt32(0), Kappa, Reader.GetString(2),
                       Reader.GetDateTime(6), Reader.GetDateTime(7), buffer[0], (byte[])Reader.GetValue(9), Reader.GetString(10));
                }
                else
                {
                    Doc = new SecretDocument(Reader.GetString(1), Reader.GetInt32(0), Kappa, Reader.GetString(2),
                       Reader.GetDateTime(6), Reader.GetDateTime(7), buffer[0], (byte[])Reader.GetValue(9), Reader.GetString(10));
                }
                ret.Add(Doc);
            }
            Connection.Close();
            return ret;
        }

        public void AddDocument(Document meme)
        {
            Connection.Open();
            SqlCommand Command = new SqlCommand("INSERT INTO DOCUMENT Values (@Num,@Name,@DigSig,@Aid,@AFir,@ASec,@DateInc,@DateOut,@Conf,@Data,@OKey)", Connection);
            Command.Parameters.Add("@Num", SqlDbType.Int).Value = meme.GetDocumentNumber();
            Command.Parameters.Add("@Name", SqlDbType.Text).Value = meme.GetDocumentName();
            Command.Parameters.Add("@DigSig", SqlDbType.Text).Value = meme.GetDigitalSignature();
            Command.Parameters.Add("@Aid", SqlDbType.Int).Value = meme.GetAuthor().id;
            Command.Parameters.Add("@AFir", SqlDbType.Text).Value = meme.GetAuthor().FirstName;
            Command.Parameters.Add("@ASec", SqlDbType.Text).Value = meme.GetAuthor().SecondName;
            Command.Parameters.Add("@DateInc", SqlDbType.DateTime).Value = meme.GetDateIncome();
            Command.Parameters.Add("@DateOut", SqlDbType.DateTime).Value = meme.GetDateOutcome();
            Command.Parameters.Add("@Conf", SqlDbType.Char, 1).Value = meme.GetConfLevel();
            Command.Parameters.Add("@Data", SqlDbType.Binary).Value = meme.GetData();
            Command.Parameters.Add("@OKey", SqlDbType.Text).Value = meme.GetOpenKeyString();
            Command.ExecuteNonQuery();
            Connection.Close();
            //SqlDataAdapter dataAdapter = new SqlDataAdapter(Command);
        }

        public Document GetDocByNumber(long num)
        {
            Connection.Open();
            SqlCommand Command = new SqlCommand("SELECT * FROM DOCUMENT WHERE DocumentNumber = @Num", Connection);
            Command.Parameters.Add("@Num", SqlDbType.Int).Value = num;
            SqlDataReader Reader = Command.ExecuteReader();
            Document Doc = null;
            if (Reader.Read())
            {
                char[] buffer = new char[1];
                Reader.GetChars(8, 0, buffer, 0, 1);
                Employee Kappa = new Employee(Reader.GetInt32(3), Reader.GetString(4), Reader.GetString(5));
                if (buffer[0] == 'N')
                {
                    Doc = new NormalDocument(Reader.GetString(1), Reader.GetInt32(0), Kappa, Reader.GetString(2),
                       Reader.GetDateTime(6), Reader.GetDateTime(7), buffer[0], (byte[])Reader.GetValue(9), Reader.GetString(10));
                }
                else
                {
                    Doc = new SecretDocument(Reader.GetString(1), Reader.GetInt32(0), Kappa, Reader.GetString(2),
                       Reader.GetDateTime(6), Reader.GetDateTime(7), buffer[0], (byte[])Reader.GetValue(9), Reader.GetString(10));
                }
            }
            Connection.Close();
            return Doc;
        }

        public void NewRequestedDate(long num)
        {
            Connection.Open();
            SqlCommand Command = new SqlCommand("UPDATE DOCUMENT SET DateOutcome = @date WHERE DocumentNumber = @Num", Connection);
            Command.Parameters.Add("@Num", SqlDbType.Int).Value = num;
            Command.Parameters.Add("@date", SqlDbType.DateTime).Value = DateTime.Now;
            Command.ExecuteNonQuery();
            Connection.Close();
        }
    }
}


