﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Ais;

namespace AisSimpleProcess
{
    public interface HistoryInterface
    {

        List<HistorySimpleStruct> ReturnHistoryAll();
        List<HistorySimpleStruct> ReturnHistoryByData(DateTime Start, DateTime Finish);
        void AddUnit(HistoryUnit NewHistEnt);
        //void AddInputUnit(HistoryUnit NewHistEnt);
        //void AddOutputUnit(HistoryUnit NewHistEnt);
    }

    //in the end it all comes to this. I need to make interfaces, but am I actually?
    //Just make ONE new HistoryWorkClass LOOOOOOOOOOOOOOOOOL 4HEad
    public class HistoryWorkClass : HistoryInterface
    {
        private History InputJournal;
        private History OutputJournal;

        public HistoryWorkClass() { InputJournal = new History('i'); OutputJournal = new History('o'); }

        public virtual List<HistorySimpleStruct> ReturnHistoryAll()
        {
            List<HistorySimpleStruct> ret = new List<HistorySimpleStruct>();
            if (InputJournal.GetAllHistory() != null)
            {
                foreach (HistoryUnit Desc in InputJournal.GetAllHistory())
                {
                    HistorySimpleStruct memes = new HistorySimpleStruct('i', Desc.GetDate(), Desc.GetDoc().GetDocumentName(), Desc.GetDoc().GetDocumentNumber());
                    ret.Add(memes);
                }
            }
            if (OutputJournal.GetAllHistory() != null)
            {
                foreach (HistoryUnit Desc in OutputJournal.GetAllHistory())
                {
                    HistorySimpleStruct memes = new HistorySimpleStruct('o', Desc.GetDate(), Desc.GetDoc().GetDocumentName(), Desc.GetDoc().GetDocumentNumber());
                    ret.Add(memes);
                }
            }
            if (ret.Count() == 0)
                return null;
            else
                return ret;
        }

        public virtual List<HistorySimpleStruct> ReturnHistoryByData(DateTime Start, DateTime Finish)
        {
            List<HistorySimpleStruct> ret = new List<HistorySimpleStruct>();
            if (InputJournal.GetAllHistory() != null)
            {
                foreach (HistoryUnit Desc in InputJournal.GetUnit(Start, Finish))
                {
                    HistorySimpleStruct memes = new HistorySimpleStruct('i', Desc.GetDate(), Desc.GetDoc().GetDocumentName(), Desc.GetDoc().GetDocumentNumber());
                    ret.Add(memes);
                }
            }
            if (OutputJournal.GetAllHistory() != null)
            {
                foreach (HistoryUnit Desc in OutputJournal.GetUnit(Start, Finish))
                {
                    HistorySimpleStruct memes = new HistorySimpleStruct('o', Desc.GetDate(), Desc.GetDoc().GetDocumentName(), Desc.GetDoc().GetDocumentNumber());
                    ret.Add(memes);
                }
            }
            if (ret.Count() == 0)
                return null;
            else
                return ret;
        }

        public virtual void AddUnit(HistoryUnit NewHistEnt)
        {
            if (NewHistEnt.GetIOType() == 'O')
                OutputJournal.AddUnit(NewHistEnt);
            else
                InputJournal.AddUnit(NewHistEnt);
        }

        
    }
}
