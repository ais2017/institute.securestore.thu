﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Ais;

namespace AisSimpleProcess
{
    public class HistoryDatabaseClass : HistoryInterface
    {
        private SqlConnection Connection;
        private SqlConnection Connection2;

        public HistoryDatabaseClass()
        {
            Connection = new SqlConnection("Server = localhost; Database = AIS; Integrated Security = true; MultipleActiveResultSets=True");
            Connection2 = new SqlConnection("Server = localhost; Database = AIS; Integrated Security = true; MultipleActiveResultSets=True");
        }

        public virtual List<HistorySimpleStruct> ReturnHistoryAll()
        {
            Connection.Open();
            Connection2.Open();
            string name = "";
            List<HistorySimpleStruct> ret = new List<HistorySimpleStruct>();
            SqlCommand Command = new SqlCommand("SELECT * FROM HISTORY", Connection);
            SqlDataReader Reader = Command.ExecuteReader();
            char[] buffer = new char[1];
            while (Reader.Read())
            {
                Reader.GetChars(1, 0, buffer, 0, 1);
                SqlCommand CommandDoc = new SqlCommand("SELECT DocumentName FROM DOCUMENT WHERE DocumentNumber = @Num", Connection2);
                CommandDoc.Parameters.Add("@Num", SqlDbType.Int).Value = Reader.GetInt32(3);
                SqlDataReader ReaderDoc = CommandDoc.ExecuteReader();
                if (ReaderDoc.Read())
                {
                    name = ReaderDoc.GetString(0);
                }
                ReaderDoc.Close();
                HistorySimpleStruct memes = new HistorySimpleStruct(buffer[0], Reader.GetDateTime(2), name, Reader.GetInt32(3));
                ret.Add(memes);
            }
            Connection.Close();
            Connection2.Close();
            if (ret.Count() == 0)
                return null;
            else
                return ret;
        }

        public virtual List<HistorySimpleStruct> ReturnHistoryByData(DateTime Start, DateTime Finish)
        {
            Connection.Open();
            Connection2.Open();
            string name = "";
            List<HistorySimpleStruct> ret = new List<HistorySimpleStruct>();
            SqlCommand Command = new SqlCommand("SELECT * FROM HISTORY WHERE hDate BETWEEN @start AND @fin", Connection);
            Command.Parameters.Add("@start", SqlDbType.DateTime).Value = Start;
            Command.Parameters.Add("@fin", SqlDbType.DateTime).Value = Finish;
            SqlDataReader Reader = Command.ExecuteReader();
            char[] buffer = new char[1];
            while (Reader.Read())
            {
                Reader.GetChars(1, 0, buffer, 0, 1);
                SqlCommand CommandDoc = new SqlCommand("SELECT DocumentName FROM DOCUMENT WHERE DocumentNumber = @Num", Connection2);
                CommandDoc.Parameters.Add("@Num", SqlDbType.Int).Value = Reader.GetInt32(3);
                SqlDataReader ReaderDoc = CommandDoc.ExecuteReader();
                if (ReaderDoc.Read())
                {
                    name = ReaderDoc.GetString(0);
                }
                ReaderDoc.Close();
                HistorySimpleStruct memes = new HistorySimpleStruct(buffer[0], Reader.GetDateTime(2), name, Reader.GetInt32(3));
                ret.Add(memes);
            }
            Connection.Close();
            Connection2.Close();
            if (ret.Count() == 0)
                return null;
            else
                return ret;
        }

        public virtual void AddUnit(HistoryUnit NewHistEnt)
        {
            Connection.Open();
            SqlCommand Command = new SqlCommand("INSERT INTO HISTORY Values (@type,@date,@num)", Connection);
            Command.Parameters.Add("@type", SqlDbType.Char).Value = NewHistEnt.GetIOType();
            Command.Parameters.Add("@date", SqlDbType.DateTime).Value = NewHistEnt.GetDate();
            Command.Parameters.Add("@num", SqlDbType.Int).Value = NewHistEnt.GetDoc().GetDocumentNumber();
            Command.ExecuteNonQuery();
            Connection.Close();
        }

    }
}
