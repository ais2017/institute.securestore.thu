﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ais;

namespace AisSimpleProcess
{
    public interface DocumentInterface
    {
        List<Document> GetAll();
        void AddDocument(Document meme);
        Document GetDocByNumber(long num);
        void NewRequestedDate(long num);
        //bool CheckDocByDoc(Document meme);
    }

    //For Document masses, or I can't Create Requests or Add Documents.
    public class DocumentWorkClass: DocumentInterface
    {
        private List<Document> DocumentList;

        public DocumentWorkClass() { DocumentList = new List<Document>(); }

        public List<Document> GetAll()
        {
            List<Document> temp = new List<Document>(DocumentList);
            return temp;
        }

        public void AddDocument(Document meme)
        {
            if (meme == null)
                throw new ArgumentNullException("Nice Document");
            DocumentList.Add(meme);
        }

        public Document GetDocByNumber(long num)
        {
            //if (DocumentList.Exists(r => r.GetDocumentNumber() == num))
            //{
                var ret = DocumentList.FirstOrDefault(r => r.GetDocumentNumber() == num);
                return ret;
           // }
           // else
           //     throw new ArgumentNullException("No document with this number");
        }

        public void NewRequestedDate(long num)
        {
            DocumentList.FirstOrDefault(r => r.GetDocumentNumber() == num).NewRequestedDate();
        }

        /*
        public bool CheckDocByDoc(Document meme)
        {
            return DocumentList.Exists(r => r == meme);
            /*
            var ret = DocumentList.FirstOrDefault(r => r == meme);
            if (ret != null) return true;
            else return false;
         }
        */
    }
}
