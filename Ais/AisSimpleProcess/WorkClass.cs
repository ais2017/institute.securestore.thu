﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Ais;

namespace AisSimpleProcess
{
    public class WorkClass: ActualInterface
    {
        private HistoryInterface IHistory;
        private DocumentInterface IDoclist;
        private RequestWorkInterface IRequest;
        protected string _publicKey;
        protected string _privateKey;
        private KeysInterface IKeys;

        //request some keys from system done
        //make tests
        //then UI
        //CmonBruh

        public WorkClass(HistoryInterface HistoryGateway, DocumentInterface DocumentGateway,
            RequestWorkInterface RequestGateway, KeysInterface KeysGateway)
        {
            IHistory = HistoryGateway;
            IDoclist = DocumentGateway;
            IRequest = RequestGateway;
            IKeys = KeysGateway;
            _publicKey = IKeys.GetOpenKey();
            _privateKey = IKeys.GetCloseKey();
        }

        public virtual void CreateNewNormalDocument(string DocumentName, long DocumentNumber, Employee Author,
            byte[] Data)
        {
            var checkvar = IDoclist.GetDocByNumber(DocumentNumber);
            if (checkvar != null)
                throw new ArgumentException("Wrong number");
            var NewNormalDocument = new NormalDocument(DocumentName, DocumentNumber, Author, DateTime.Now, DateTime.Now, 'N', Data, _publicKey, _privateKey);
            HistoryUnit NewHistEnt = new HistoryUnit('i', NewNormalDocument);
            this.IDoclist.AddDocument(NewNormalDocument);
            this.IHistory.AddUnit(NewHistEnt);
        }

        public virtual void CreateNewSecretDocument(string DocumentName, long DocumentNumber, Employee Author,
            byte[] Data)
        {
            var checkvar = IDoclist.GetDocByNumber(DocumentNumber);
            if (checkvar != null)
                throw new ArgumentException("Wrong number");
            var NewSecretDocument = new SecretDocument(DocumentName, DocumentNumber, Author, DateTime.Now, DateTime.Now, 'C', Data, _publicKey, _privateKey);
            HistoryUnit NewHistEnt = new HistoryUnit('i', NewSecretDocument);
            this.IDoclist.AddDocument(NewSecretDocument);
            this.IHistory.AddUnit(NewHistEnt);
        }

        public virtual List<HistorySimpleStruct> ReturnHistoryAll()
        {
            return IHistory.ReturnHistoryAll();
        }

        public virtual List<HistorySimpleStruct> ReturnHistoryByData(DateTime Start, DateTime Finish)
        {
            return IHistory.ReturnHistoryByData(Start, Finish);
        }

        public virtual void CreateRequest(long number, Employee Source, string Key)
        {
            var Doc = IDoclist.GetDocByNumber(number);
            if (Doc == null)
                throw new ArgumentNullException("There is no such Document");
            var NewRequest = new Request(Doc, Key, DateTime.Now, Source);
            if (Doc.GetConfLevel() == 'N')
                NewRequest.Approve();       //autoconfirm
            IRequest.AddRequest(NewRequest);
            var NewHistEnt = new HistoryUnit('o', Doc);
            IHistory.AddUnit(NewHistEnt);
            IDoclist.NewRequestedDate(number);
            Doc.NewRequestedDate();
        }

        public List<DocumentSimpleStruct> ReturnSimpleDocumentsAll()
        {
            List<DocumentSimpleStruct> Kapp = new List<DocumentSimpleStruct>();
            List<Document> BDCD = IDoclist.GetAll();
            if (BDCD != null)
            {
                foreach (Document Desc in BDCD)
                {
                    DocumentSimpleStruct memes = new DocumentSimpleStruct(Desc);
                    Kapp.Add(memes);
                }
            }
            if (Kapp.Count() == 0)
                return null;
            else
                return Kapp;
        }

        public List<RequestSimpleStruct> ReturnRequests()
        {
            List<RequestSimpleStruct> Kapp = new List<RequestSimpleStruct>();
            List<ReqStruct> BDC = IRequest.GetAll();
            if (BDC != null)
            {
                foreach (ReqStruct Desc in BDC)
                {
                    RequestSimpleStruct memes = new RequestSimpleStruct(Desc);
                    Kapp.Add(memes);
                }
            }
            if (Kapp.Count() == 0)
                return null;
            else
                return Kapp;
        }

        public List<RequestSimpleStruct> ReturnRequestsForEmployee(int ID)
        {
            List<RequestSimpleStruct> Kapp = new List<RequestSimpleStruct>();
            List<ReqStruct> BDC = IRequest.GetRequestsBySource(ID);
            if (BDC != null)
            {
                foreach (ReqStruct Desc in BDC)
                {
                    RequestSimpleStruct memes = new RequestSimpleStruct(Desc);
                    Kapp.Add(memes);
                }
            }
            if (Kapp.Count() == 0)
                return null;
            else
                return Kapp;
        }

        public byte[] GetDocumentByRequest(long Ident)
        {
            byte[] ByteData;
            Request temp = IRequest.GetRequestByNumber(Ident);
            if (temp.GetStatus() != Stat.Approved)
                throw new ArgumentException("Request is not Approved");
            if (temp.GetDoc().GetConfLevel() == 'N')
                ByteData = temp.GetDoc().GetData();
            else
                ByteData = temp.GetDoc().GetData(temp.GetCloseKey());
            return ByteData;
        }

        public bool CheckLocalSign(long Ident)
        {
            var Doc = IDoclist.GetDocByNumber(Ident);
            if (Doc == null)
                throw new ArgumentNullException("There is no such Document");
            return Doc.CheckDS(_publicKey);
        }

        public virtual bool CheckRequestSign(long Ident)
        {
            if (!IRequest.CheckRequestByNumber(Ident))
                throw new ArgumentException("Fake, thats the wrong number");
            return IRequest.GetRequestByNumber(Ident).GetDoc().CheckDS(IRequest.GetRequestByNumber(Ident).GetCloseKey());
        }

        public virtual void ApproveRequest(long Ident)
        {
            if (!IRequest.CheckRequestByNumber(Ident))
                throw new ArgumentException("Fake, thats the wrong number");
            IRequest.ApproveRequest(Ident);
            //IRequest.GetRequestByNumber(Ident).Approve();
        }

        public virtual void DeclineRequest(long Ident)
        {
            if (!IRequest.CheckRequestByNumber(Ident))
                throw new ArgumentException("Fake, thats the wrong number");
            IRequest.DeclineRequest(Ident);
            //IRequest.GetRequestByNumber(Ident).Decline();         - too long, less dependand, who cares
        }

        //For tests!
        public virtual string GetOpenKey() { return IKeys.GetOpenKey(); }
        public virtual string GetCloseKey() { return IKeys.GetCloseKey(); }
    }
}