﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Ais;

namespace AisSimpleProcess
{
    public class RequestDatabaseClass: RequestWorkInterface
    {
        private SqlConnection Connection;
        private SqlConnection Connection2;

        public RequestDatabaseClass()
        {
            //MAGI-31-MAIN
            Connection = new SqlConnection("Server = localhost; Database = AIS; Integrated Security = true; MultipleActiveResultSets=True");
            Connection2 = new SqlConnection("Server = localhost; Database = AIS; Integrated Security = true; MultipleActiveResultSets=True");
        }

        public List<ReqStruct> GetAll()
        {
            Connection.Open();
            Connection2.Open();
            List<ReqStruct> KappList = new List<ReqStruct>();
            Stat TempStat = Stat.Considered;
            ReqStruct Kapp = new ReqStruct();
            Employee KappaPride = null;
            Employee KappaRoss = null;
            Document TempDoc = null;
            SqlCommand Command = new SqlCommand("SELECT * FROM REQUESTS", Connection);
            SqlDataReader Reader = Command.ExecuteReader();
            //SqlDataReader ReaderDoc = CommandDoc.ExecuteReader();
            while (Reader.Read())
            {
                Kapp.ReqNumber = Reader.GetInt32(0);
                KappaPride = new Employee(Reader.GetInt32(4), Reader.GetString(5), Reader.GetString(6));
                SqlCommand CommandDoc = new SqlCommand("SELECT * FROM DOCUMENT WHERE DocumentNumber = @Num", Connection2);
                CommandDoc.Parameters.Add("@Num", SqlDbType.Int).Value = Reader.GetInt32(1);
                switch (Reader.GetString(7))
                {
                    case "Considered":
                        TempStat = Stat.Considered; break;
                    case "Approved":
                        TempStat = Stat.Approved; break;
                    case "Declined":
                        TempStat = Stat.Declined; break;
                }
                SqlDataReader ReaderDoc = CommandDoc.ExecuteReader();
                //who needs functions anyway
                if (ReaderDoc.Read())
                {
                    char[] buffer = new char[1];
                    ReaderDoc.GetChars(8, 0, buffer, 0, 1);
                    KappaRoss = new Employee(ReaderDoc.GetInt32(3), ReaderDoc.GetString(4), ReaderDoc.GetString(5));
                    if (buffer[0] == 'N')
                    {
                        TempDoc = new NormalDocument(ReaderDoc.GetString(1), ReaderDoc.GetInt32(0), KappaRoss, ReaderDoc.GetString(2),
                           ReaderDoc.GetDateTime(6), ReaderDoc.GetDateTime(7), buffer[0], (byte[])ReaderDoc.GetValue(9), ReaderDoc.GetString(10));
                    }
                    else
                    {
                        TempDoc = new SecretDocument(ReaderDoc.GetString(1), ReaderDoc.GetInt32(0), KappaRoss, ReaderDoc.GetString(2),
                           ReaderDoc.GetDateTime(6), ReaderDoc.GetDateTime(7), buffer[0], (byte[])ReaderDoc.GetValue(9), ReaderDoc.GetString(10));
                    }
                }
                else
                    throw new ArgumentException("Nice Document"); //Dunno, it will never execute
                ReaderDoc.Close();
                Kapp.ReqLink = new Request(TempDoc, Reader.GetString(2), Reader.GetDateTime(3), KappaPride, TempStat);
                KappList.Add(Kapp);
            }
            Connection.Close();
            Connection2.Close();
            if (KappList.Count == 0)
                return null;
            return KappList;
        }

        public void AddRequest(Request meme)
        {
            Connection.Open();
            SqlCommand Command = new SqlCommand("INSERT INTO REQUESTS Values (@DocNum,@CKey,@Date,@Aid,@AFir,@Asec,@Stat)", Connection);
            Command.Parameters.Add("@DocNum", SqlDbType.Int).Value = meme.GetDoc().GetDocumentNumber();
            Command.Parameters.Add("@CKey", SqlDbType.Text).Value = meme.GetCloseKey();
            Command.Parameters.Add("@Date", SqlDbType.DateTime).Value = meme.GetDateRequest();
            Command.Parameters.Add("@Aid", SqlDbType.Int).Value = meme.GetSource().id;
            Command.Parameters.Add("@AFir", SqlDbType.Text).Value = meme.GetSource().FirstName;
            Command.Parameters.Add("@ASec", SqlDbType.Text).Value = meme.GetSource().SecondName;
            switch (meme.GetStatus())
            {
                case Stat.Considered:
                    Command.Parameters.Add("@Stat", SqlDbType.Text).Value = "Considered";
                    break;
                case Stat.Approved:
                    Command.Parameters.Add("@Stat", SqlDbType.Text).Value = "Approved";
                    break;
                case Stat.Declined:
                    Command.Parameters.Add("@Stat", SqlDbType.Text).Value = "Declined";
                    break;
            }
            Command.ExecuteNonQuery();
            Connection.Close();
        }

        public Request GetRequestByNumber(long num)
        {
            Connection.Open();
            Connection2.Open();
            Request ret = null;
            Stat TempStat = Stat.Considered;
            Document TempDoc = null;
            Employee KappaPride = null;
            Employee KappaRoss = null;
            SqlCommand Command = new SqlCommand("SELECT * FROM REQUESTS WHERE requestID = @Num", Connection);
            Command.Parameters.Add("@Num", SqlDbType.Int).Value = num;
            SqlDataReader Reader = Command.ExecuteReader();
            if (Reader.Read())
            {
                KappaPride = new Employee(Reader.GetInt32(4), Reader.GetString(5), Reader.GetString(6));
                SqlCommand CommandDoc = new SqlCommand("SELECT * FROM DOCUMENT WHERE DocumentNumber = @Num", Connection2);
                CommandDoc.Parameters.Add("@Num", SqlDbType.Int).Value = Reader.GetInt32(1);
                switch (Reader.GetString(7))
                {
                    case "Considered":
                        TempStat = Stat.Considered; break;
                    case "Approved":
                        TempStat = Stat.Approved; break;
                    case "Declined":
                        TempStat = Stat.Declined; break;
                }
                SqlDataReader ReaderDoc = CommandDoc.ExecuteReader();
                if (ReaderDoc.Read())
                {
                    char[] buffer = new char[1];
                    ReaderDoc.GetChars(8, 0, buffer, 0, 1);
                    KappaRoss = new Employee(ReaderDoc.GetInt32(3), ReaderDoc.GetString(4), ReaderDoc.GetString(5));
                    if (buffer[0] == 'N')
                    {
                        TempDoc = new NormalDocument(ReaderDoc.GetString(1), ReaderDoc.GetInt32(0), KappaRoss, ReaderDoc.GetString(2),
                           ReaderDoc.GetDateTime(6), ReaderDoc.GetDateTime(7), buffer[0], (byte[])ReaderDoc.GetValue(9), ReaderDoc.GetString(10));
                    }
                    else
                    {
                        TempDoc = new SecretDocument(ReaderDoc.GetString(1), ReaderDoc.GetInt32(0), KappaRoss, ReaderDoc.GetString(2),
                           ReaderDoc.GetDateTime(6), ReaderDoc.GetDateTime(7), buffer[0], (byte[])ReaderDoc.GetValue(9), ReaderDoc.GetString(10));
                    }
                }
                else
                    throw new ArgumentException("Nice Document"); //Dunno, it will never execute
                ReaderDoc.Close();
                ret = new Request(TempDoc, Reader.GetString(2), Reader.GetDateTime(3), KappaPride, TempStat);
            }
            Connection.Close();
            Connection2.Close();
            return ret;
        }
        
        public List<ReqStruct> GetRequestsBySource(long num)
        {
            Connection.Open();
            Connection2.Open();
            List<ReqStruct> KappList = new List<ReqStruct>();
            Stat TempStat = Stat.Considered;
            ReqStruct Kapp = new ReqStruct();
            Employee KappaPride = null;
            Employee KappaRoss = null;
            Document TempDoc = null;
            SqlCommand Command = new SqlCommand("SELECT * FROM REQUESTS WHERE AuthorID = @Num", Connection);
            Command.Parameters.Add("@Num", SqlDbType.Int).Value = num;
            SqlDataReader Reader = Command.ExecuteReader();
            //SqlDataReader ReaderDoc = CommandDoc.ExecuteReader();
            while (Reader.Read())
            {
                Kapp.ReqNumber = Reader.GetInt32(0);
                KappaPride = new Employee(Reader.GetInt32(4), Reader.GetString(5), Reader.GetString(6));
                SqlCommand CommandDoc = new SqlCommand("SELECT * FROM DOCUMENT WHERE DocumentNumber = @Num", Connection2);
                CommandDoc.Parameters.Add("@Num", SqlDbType.Int).Value = Reader.GetInt32(1);
                switch (Reader.GetString(7))
                {
                    case "Considered":
                        TempStat = Stat.Considered; break;
                    case "Approved":
                        TempStat = Stat.Approved; break;
                    case "Declined":
                        TempStat = Stat.Declined; break;
                }
                SqlDataReader ReaderDoc = CommandDoc.ExecuteReader();
                if (ReaderDoc.Read())
                {
                    char[] buffer = new char[1];
                    ReaderDoc.GetChars(8, 0, buffer, 0, 1);
                    KappaRoss = new Employee(ReaderDoc.GetInt32(3), ReaderDoc.GetString(4), ReaderDoc.GetString(5));
                    if (buffer[0] == 'N')
                    {
                        TempDoc = new NormalDocument(ReaderDoc.GetString(1), ReaderDoc.GetInt32(0), KappaRoss, ReaderDoc.GetString(2),
                           ReaderDoc.GetDateTime(6), ReaderDoc.GetDateTime(7), buffer[0], (byte[])ReaderDoc.GetValue(9), ReaderDoc.GetString(10));
                    }
                    else
                    {
                        TempDoc = new SecretDocument(ReaderDoc.GetString(1), ReaderDoc.GetInt32(0), KappaRoss, ReaderDoc.GetString(2),
                           ReaderDoc.GetDateTime(6), ReaderDoc.GetDateTime(7), buffer[0], (byte[])ReaderDoc.GetValue(9), ReaderDoc.GetString(10));
                    }
                }
                else
                    throw new ArgumentException("Nice Document"); //Dunno, it will never execute
                ReaderDoc.Close();
                Kapp.ReqLink = new Request(TempDoc, Reader.GetString(2), Reader.GetDateTime(3), KappaPride, TempStat);
                KappList.Add(Kapp);
            }
            Connection.Close();
            Connection2.Close();
            if (KappList.Count == 0)
                return null;
            return KappList;
        }
        
        public bool CheckRequestByNumber(long num)
        {
            Connection.Open();
            SqlCommand Command = new SqlCommand("SELECT count(*) FROM REQUESTS WHERE requestID = @Num", Connection);
            Command.Parameters.Add("@Num", SqlDbType.Int).Value = num;
            SqlDataReader ReaderDoc = Command.ExecuteReader();
            if (ReaderDoc.Read())
            {
                if (ReaderDoc.GetInt32(0) == 0)
                {
                    Connection.Close();
                    return false;
                }
                Connection.Close();
                return true;
            }
            Connection.Close();
            return false;
        }

        public void ApproveRequest(long num)
        {
            Connection.Open();
            SqlCommand Command = new SqlCommand("SELECT * FROM REQUESTS WHERE requestID = @Num", Connection);
            Command.Parameters.Add("@Num", SqlDbType.Int).Value = num;
            SqlDataReader ReaderDoc = Command.ExecuteReader();
            if (ReaderDoc.Read())
            {
                if (ReaderDoc.GetString(7) != "Considered")
                    throw new ArgumentException("Nice Request");
            }
            Command = new SqlCommand("UPDATE REQUESTS SET Stat = 'Approved' WHERE requestID = @Num", Connection);
            Command.Parameters.Add("@Num", SqlDbType.Int).Value = num;
            Command.ExecuteNonQuery();
            Connection.Close();
        }

        public void DeclineRequest(long num)
        {
            Connection.Open();
            SqlCommand Command = new SqlCommand("SELECT * FROM REQUESTS WHERE requestID = @Num", Connection);
            Command.Parameters.Add("@Num", SqlDbType.Int).Value = num;
            SqlDataReader ReaderDoc = Command.ExecuteReader();
            ReaderDoc.Read();
            if (ReaderDoc.Read())
            {
                if (ReaderDoc.GetString(7) != "Considered")
                    throw new ArgumentException("Nice Request");
            }
            Command = new SqlCommand("UPDATE REQUESTS SET Stat = 'Declined' WHERE requestID = @Num", Connection);
            Command.Parameters.Add("@Num", SqlDbType.Int).Value = num;
            Command.ExecuteNonQuery();
            Connection.Close();
        }

    }
}
